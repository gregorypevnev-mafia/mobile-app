import { toDate, toTime } from "../../src/utilities/time";

const TIME = 15000000000;

describe("Time tests", () => {
  it("should display date", () => {
    expect(toDate(TIME)).toBe("23 June 1970");
  });

  it("should display time", () => {
    expect(toTime(TIME)).toMatch(":40"); // Hours may not match on Mac / Linux
  });
})