import { createNormalizer, emptyState } from "../../src/utilities/normalization";
import { ITEM1, ITEM2, ITEM3, FULL_STATE, SINGLE_STATE, EMPTY_STATE } from "./mockData";

const NEW_NAME = "George";

const EXTRACTOR = item => String(item.id);

const errorFunction = _ => { throw new Error("Should NOT run") };

describe("Normalization Tests", () => {
  it("should initialize state", () => {
    const normalizer = createNormalizer({ extractor: EXTRACTOR });

    let state;

    state = normalizer.merge(state, [ITEM1, ITEM2]);

    expect(state).toEqual(FULL_STATE);
  });

  it("should merge state", () => {
    const normalizer = createNormalizer({ extractor: EXTRACTOR });

    let state;

    state = normalizer.merge(state, [ITEM1]);

    state = normalizer.merge(state, [ITEM2]);

    state = normalizer.merge(state, [ITEM1]);

    expect(state).toEqual(FULL_STATE);
  });

  it("should replace state", () => {
    const normalizer = createNormalizer({ extractor: EXTRACTOR });

    let state;

    state = normalizer.replace(state, [ITEM1, ITEM3]);

    state = normalizer.replace(state, [ITEM2, ITEM3]);

    state = normalizer.replace(state, [ITEM1, ITEM2]);

    expect(state).toEqual(FULL_STATE);
  });

  it("should put items", () => {
    const normalizer = createNormalizer({ extractor: EXTRACTOR });

    let state;

    state = normalizer.put(state, ITEM2);

    state = normalizer.put(state, ITEM1);

    expect(state).toEqual(FULL_STATE);
  });

  it("should merge and put", () => {
    const normalizer = createNormalizer({ extractor: EXTRACTOR });

    let state;

    state = normalizer.put(state, ITEM2);

    state = normalizer.merge(state, [ITEM1, ITEM2]);

    state = normalizer.put(state, ITEM1);

    expect(state).toEqual(FULL_STATE);
  });

  it("should remove items", () => {
    const normalizer = createNormalizer({ extractor: EXTRACTOR });

    let state;

    state = normalizer.merge(state, [ITEM1, ITEM2, ITEM3]);

    state = normalizer.remove(state, ITEM3.id);

    state = normalizer.remove(state, ITEM3.id);

    expect(state).toEqual(FULL_STATE);
  });

  it("should update items", () => {
    const normalizer = createNormalizer({ extractor: EXTRACTOR });

    let state;

    state = normalizer.merge(state, [ITEM1, ITEM2]);

    state = normalizer.update(state, ITEM2.id, item => ({ ...item, name: NEW_NAME }));

    expect(state.entities[ITEM2.id].name).toBe(NEW_NAME)

    state = normalizer.update(state, ITEM2.id, item => ({ ...item, name: "Max" }));

    state = normalizer.update(state, ITEM3.id, errorFunction);

    expect(state).toEqual(FULL_STATE);
  });

  it("should avoid duplication", () => {
    const normalizer = createNormalizer({ extractor: EXTRACTOR });

    let state;

    state = normalizer.put(state, ITEM1);

    state = normalizer.put(state, ITEM2);

    state = normalizer.merge(state, [ITEM1, ITEM2]);

    state = normalizer.merge(state, [ITEM2, ITEM1]);

    state = normalizer.put(state, ITEM2);

    state = normalizer.put(state, ITEM1);

    expect(state).toEqual(FULL_STATE);
  });

  it("should use empty state", () => {
    const normalizer = createNormalizer();

    expect(normalizer.merge(undefined, [ITEM1, ITEM2])).toEqual(FULL_STATE);

    expect(normalizer.put(undefined, ITEM1)).toEqual(SINGLE_STATE);

    expect(normalizer.remove(undefined, ITEM1.id)).toEqual(EMPTY_STATE);

    expect(normalizer.update(undefined, ITEM1.id, errorFunction)).toEqual(EMPTY_STATE);
  });
});