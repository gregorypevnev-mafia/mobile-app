import { createDenormalizer } from "../../src/utilities/normalization";
import { ITEM1, ITEM2, FULL_STATE } from "./mockData";

const CATEGORY_ID = "ID";

const CATEGORIZER_NAME = item => item.name.slice(0, 1);

const CATEGORIZER_ID = _ => CATEGORY_ID;

describe("Denormalization Tests", () => {
  it("should list items", () => {
    const denormalizer = createDenormalizer();

    expect(denormalizer.list(FULL_STATE)).toEqual([ITEM1, ITEM2]);
  });

  it("should categorize items", () => {
    const denormalizer = createDenormalizer({ categorizer: CATEGORIZER_NAME });

    expect(denormalizer.categories(FULL_STATE)).toEqual({
      [CATEGORIZER_NAME(ITEM1)]: [ITEM1],
      [CATEGORIZER_NAME(ITEM2)]: [ITEM2],
    });
  });

  it("should categorize items (Same category)", () => {
    const denormalizer = createDenormalizer({ categorizer: CATEGORIZER_ID });

    expect(denormalizer.categories(FULL_STATE)).toEqual({
      [CATEGORY_ID]: [ITEM1, ITEM2],
    });
  });

  it("should categorize by default", () => {
    const denormalizer = createDenormalizer();

    expect(denormalizer.categories(FULL_STATE)).toEqual({
      "DEFAULT": [ITEM1, ITEM2],
    });
  });
});
