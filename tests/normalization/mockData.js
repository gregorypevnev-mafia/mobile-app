export const ITEM1 = {
  id: "1",
  name: "Greg"
};

export const ITEM2 = {
  id: "2",
  name: "Max"
};

export const ITEM3 = {
  id: "3",
  name: "Alex"
};

export const FULL_STATE = {
  entities: {
    [ITEM1.id]: ITEM1,
    [ITEM2.id]: ITEM2
  },
  list: [ITEM1.id, ITEM2.id]
};

export const SINGLE_STATE = {
  entities: {
    [ITEM1.id]: ITEM1
  },
  list: [ITEM1.id]
};

export const EMPTY_STATE = {
  entities: {},
  list: []
};
