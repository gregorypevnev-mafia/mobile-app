import { emptyState } from "../../src/utilities/normalization";
import { EMPTY_STATE } from "./mockData";

describe("State tests", () => {
  it("should initialize empty state", () => {
    expect(emptyState()).toEqual(EMPTY_STATE);
  });
})