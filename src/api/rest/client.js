import axios from "axios";
import { authHeaders } from "../common";

const attachAuthData = (req, token) => {
  req.headers = {
    ...req.headers,
    ...authHeaders(token),
  };

  return req;
};

// Detail - Does NOT depend on Store, acts by itself
// Pattern: Dependencies / Details -> Settings / Parameters
const createClient = ({ url }, { loadToken }, { onAuthenticated, onUnauthenicated }) => {
  const client = axios.create({
    baseURL: url,
    timeout: 2000,
    // withCredentials: false, // CORS
  });

  client.interceptors.request.use(async req => {
    const token = await loadToken();

    if (token) return attachAuthData(req, token);

    return req;
  });

  // Important: Separate interceptors for Successed and Failed responses
  client.interceptors.response.use(
    res => res,
    async error => {
      if (error.response.status === 401) onUnauthenicated();

      return Promise.reject(error);
    }
  );

  return {
    authCall(caller) {
      const authCaller = caller(client);

      return async (...args) => {
        const { token, user } = await authCaller(...args);

        onAuthenticated({ token, user });

        return { user };
      }
    },
    unauthCall(caller) {
      const unauthCaller = caller(client);

      return async (...args) => {
        const result = await unauthCaller(...args);

        onUnauthenicated(result);

        return result;
      }
    },
    call(caller) {
      return caller(client);
    }
  }
};

export default createClient;