const DEFAULT_MESSAGE = "Could not make request";

const formatErrorData = ({ error: { message } }) => message;

const formatValidationErrors = ({ message, errors }) => `${message}: ${errors[0]}`

const extractErrorMessage = errorData => {
  if (errorData.error) return formatErrorData(errorData);

  if (errorData.errors) return formatValidationErrors(errorData);

  return null;
}

export const errorFromResponse = (error, defaultMessage = DEFAULT_MESSAGE) => {
  const isApplicationError = (error.response && error.response.data);

  const errorMessage = isApplicationError ? extractErrorMessage(error.response.data) : null;

  return {
    message: errorMessage || defaultMessage
  };
};
