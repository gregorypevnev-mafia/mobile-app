const JSON_TYPE = "application/json";
const IMAGE_TYPE = "image/jpeg";
const FORMDATA_TYPE = "multipart/form-data";

const TYPE_HEADER = "Content-Type";

const fileField = type => (uri, name) => (
  uri ? ({
    uri,
    name,
    type
  }) : null
);

const formFields = fields =>
  Object.keys(fields).reduce((data, field) => {
    data.append(field, fields[field]);

    return data;
  }, new FormData());

const jsonFields = fields => JSON.stringify(fields);

const config = type => () => ({
  headers: {
    [TYPE_HEADER]: type,
  }
});

const jsonConfig = config(JSON_TYPE);
const formConfig = config(FORMDATA_TYPE);

export const imageData = fileField(IMAGE_TYPE);

export const jsonData = fields => [
  jsonFields(fields),
  jsonConfig(),
];

export const formData = fields => [
  formFields(fields),
  formConfig(),
];
