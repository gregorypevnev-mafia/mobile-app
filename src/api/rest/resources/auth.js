import { errorFromResponse } from "../utils/errors";
import { jsonData, formData, imageData } from "../utils/data";

const IMAGE_NAME = "profile.jpg";

export const signIn = client => async signInData => {
  try {
    const { data: { user, token } } = await client.post("/auth/signin", ...jsonData(signInData));

    return {
      user,
      token,
    };
  } catch (e) {
    throw errorFromResponse(e, "Invalid username or password");
  }
};

export const signUp = client => async ({ username, password, image }) => {
  try {
    const signUpData = {
      username,
      password,
      image: imageData(image, IMAGE_NAME),
    };

    const { data: { user, token } } = await client.post("/auth/signup", ...formData(signUpData));

    return {
      user,
      token,
    };
  } catch (e) {
    throw errorFromResponse(e, "Could not sign up");
  }
};

export const signOut = client => async () => {
  return client.delete("/auth/signout").catch(() => null);
};
