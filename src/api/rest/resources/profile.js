export const loadProfile = client => async () => {
  try {
    const { data: { user } } = await client.get("/profile");

    return { user };
  } catch (e) {
    throw { message: "Could not load profile" };
  }
};