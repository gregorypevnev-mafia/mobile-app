const extractData = ({
  profile: { user },
  friends: { friends, requests },
  games: { games, invitations },
}) => ({
  user,
  friends,
  requests,
  games,
  invitations,
});

export const load = client => async () => {
  try {
    const { data } = await client.get("/dashboard");

    return extractData(data);
  } catch (e) {
    throw { message: "Could not load games and invitations" };
  }
};