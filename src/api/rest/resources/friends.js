import { errorFromResponse } from "../utils/errors";
import { jsonData } from "../utils/data";

export const loadFriends = client => async () => {
  try {
    const { data: { friends, requests } } = await client.get("/friends");

    return { friends, requests };
  } catch (e) {
    throw { message: "Could not load friends and friend requests" };
  }
};

export const acceptRequest = client => async requestId => {
  try {
    const { data: { friend } } = await client.put(`/requests/${requestId}`);

    return { friend };
  } catch (e) {

    throw { message: "Could not accept friend request" };
  }
};

export const rejectRequest = client => async requestId => {
  try {
    await client.delete(`/requests/${requestId}`);
  } catch (e) {
    throw { message: "Could not reject friend request" };
  }
};

export const requestFriend = client => async friendData => {
  try {
    // Axios does not how to provide json in React-Native automatically - SUCKS
    await client.post("/requests", ...jsonData(friendData));
  } catch (e) {
    throw errorFromResponse(e, "Could not send a friend request");
  }
};

export const deleteFriend = client => async gameId => {
  try {
    await client.delete(`/friends/${gameId}`);
  } catch (e) {
    throw { message: "Could not unfriend" };
  }
};