import { errorFromResponse } from "../utils/errors";
import { jsonData } from "../utils/data";

export const loadGames = client => async () => {
  try {
    const { data: { games, invitations } } = await client.get("/games");

    return { games, invitations };
  } catch (e) {
    throw { message: "Could not load games and invitations" };
  }
};

export const loadDetails = client => async gameId => {
  try {
    const { data: { game } } = await client.get(`/games/${gameId}`);

    return { game };
  } catch (e) {
    throw { message: "Could not load game's details" };
  }
};

export const acceptInvitation = client => async invitationId => {
  try {
    const { data: { game } } = await client.put(`/invitations/${invitationId}`);

    return { game };
  } catch (e) {
    throw { message: "Could not accept invitation" };
  }
};

export const rejectInvitation = client => async invitationId => {
  try {
    await client.delete(`/invitations/${invitationId}`);
  } catch (e) {
    throw { message: "Could not reject invitation" };
  }
};

export const createGame = client => async (name, invitations, settings) => {
  try {
    const gameData = {
      name,
      invitations,
      settings,
    };

    const { data: { game } } = await client.post("/games", ...jsonData(gameData));

    return { game };
  } catch (e) {
    throw errorFromResponse(e, "Could not create a game");
  }
};

export const deleteGame = client => async gameId => {
  try {
    await client.delete(`/games/${gameId}`);
  } catch (e) {
    throw { message: "Could not delete game" };
  }
};
