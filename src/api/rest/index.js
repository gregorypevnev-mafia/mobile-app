import createClient from "./client";
import { signIn, signOut, signUp } from "./resources/auth";
import { load } from "./resources/dashboard";
import { loadProfile } from "./resources/profile";
import { loadGames, loadDetails, acceptInvitation, rejectInvitation, createGame, deleteGame } from "./resources/games";
import { loadFriends, acceptRequest, rejectRequest, requestFriend, deleteFriend } from "./resources/friends";

// Detail - Does NOT depend on Store, acts by itself
// Pattern: Dependencies / Details -> Settings / Parameters
const createREST = ({ url }, { loadToken }) => {
  const onAuthListeners = [];
  const onUnauthListeners = [];

  const client = createClient({ url }, { loadToken }, {
    onAuthenticated({ user, token }) {
      onAuthListeners.forEach(listener => listener({ user, token }));
    },
    onUnauthenicated() {
      onUnauthListeners.forEach(listener => listener());
    }
  });

  return {
    restApi: {
      auth: {
        signIn: client.authCall(signIn),
        signUp: client.authCall(signUp),
        signOut: client.unauthCall(signOut),
      },
      dashboard: {
        load: client.call(load),
      },
      profile: {
        load: client.call(loadProfile),
      },
      games: {
        load: client.call(loadGames),
        loadDetails: client.call(loadDetails),
        create: client.call(createGame),
        delete: client.call(deleteGame),
        acceptInvitation: client.call(acceptInvitation),
        rejectInvitation: client.call(rejectInvitation),
      },
      friends: {
        load: client.call(loadFriends),
        request: client.call(requestFriend),
        delete: client.call(deleteFriend),
        acceptRequest: client.call(acceptRequest),
        rejectRequest: client.call(rejectRequest),
      }
    },
    events: {
      onAuth(listener) {
        onAuthListeners.push(listener);
      },
      onUnauth(listener) {
        onUnauthListeners.push(listener);
      },
    }
  }
};

export default createREST;