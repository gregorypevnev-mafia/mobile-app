const TOKEN_TYPE = "JWT";
const REQUEST_AUTH_HEADER = "Authorization";

export const authHeaders = token => ({
  [REQUEST_AUTH_HEADER]: `${TOKEN_TYPE} ${token}`
});
