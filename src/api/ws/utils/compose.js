export const composeEvents = (...eventListeners) =>
  payload => eventListeners.forEach(listener => listener(payload));