import { authHeaders } from "../../common";

const extractTicket = ({ ticket }) => ticket || null;

export const getTicket = (url, token) =>
  fetch(url, {
    method: "POST",
    headers: authHeaders(token),
  }).then(res => {
    if (res.status === 200)
      return res.json();

    throw new Error();
  })
    .then(extractTicket)
    .catch(() => null);
