const joinRoom = "join-room";
const leaveRoom = "leave-room";

export const messages = {
  joinRoom,
  leaveRoom,
};
