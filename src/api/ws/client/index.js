import { createWebSocketClient } from "./client";
import { getTicket } from "./tickets";

const ticketFetcher = loadToken => url => async () => {
  const token = await loadToken();

  const ticket = await getTicket(url, token);

  return ticket;
};

const listenersNotifier = notifiers => message => {
  const { type, payload } = JSON.parse(message);

  notifiers.forEach(notifier => notifier(type, payload));
}

export const createClient = ({ loadToken }, { wsURL, ticketURL }) => {
  const listeners = [];

  const client = createWebSocketClient(
    { url: wsURL },
    { getTicket: ticketFetcher(loadToken)(ticketURL) },
    { onMessage: listenersNotifier(listeners) }
  );

  return {
    messenger(messenger) {
      return messenger(client);
    },
    onMessage(eventListener) {
      listeners.push(eventListener);
    },
    connect() {
      return client.connect();
    },
    disconnect() {
      return client.disconnect();
    }
  }
};
