const connectionURI = (url, ticket) => `${url}?ticket=${ticket}`;

export const openWebSocket = (url, { ticket }) =>
  new Promise((res, rej) => {
    const socket = new WebSocket(connectionURI(url, ticket));

    socket.onopen = () => {
      socket.onopen = null;

      res(socket);
    };

    socket.onerror = () => {
      rej();
    };
  });
