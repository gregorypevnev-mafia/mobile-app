import { openWebSocket } from "./sockets";
import { messages } from "./messages";

const SWITCHING_DELAY = 1000;

const getMessage = (type, payload = {}) => JSON.stringify({
  type,
  payload,
});

export const createWebSocketClient = ({ url }, { getTicket }, { onMessage }) => {
  let socket;

  return {
    async connect() {
      const ticket = await getTicket();

      if (ticket === null) return; // Cannot connect

      socket = await openWebSocket(url, { ticket });

      socket.onmessage = ({ data }) => onMessage(data);
    },
    disconnect() {
      if (!socket) return;

      socket.close();
    },
    joinRoom(room) {
      if (!socket) return Promise.resolve();

      return new Promise((res, rej) => {
        socket.send(getMessage(messages.joinRoom, { room }));

        setTimeout(res, SWITCHING_DELAY);
      });
    },
    leaveRoom() {
      if (!socket) return Promise.resolve();

      return new Promise((res, rej) => {
        socket.send(getMessage(messages.leaveRoom));

        setTimeout(res, SWITCHING_DELAY);
      });
    },
    send(type, payload) {
      if (!socket) return;

      socket.send(getMessage(type, payload));
    }
  }
};