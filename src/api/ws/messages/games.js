export const join = client => async (gameId, username) => {
  // IMPORTANT: Sending AFTER Joining to take advantage of Metadata
  await client.joinRoom(gameId);

  client.send("join-game", { username });
};

export const leave = client => async () => {
  // IMPORTANT: Sending BEFORE Leaving to take advantage of Metadata
  await client.send("leave-game");

  client.leaveRoom();
};
