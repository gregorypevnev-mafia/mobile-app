import { join, leave } from "./games";
import { sendMessage } from "./messages";
import { sendVote } from "./votes";

export default {
  joinGame: join,
  leaveGame: leave,
  send: sendMessage,
  vote: sendVote
};
