import {
  message as publishMessage,
  info as publishInfo,
} from "../../../store/messages/actions";
import { isActive } from "../../../utilities/game";


export const message = (dispatch, state) => ({ message }) => {
  const { play, user: { user } } = state();

  const currentPlayer = play.players[user.id];

  if (isActive(play, currentPlayer)) dispatch(publishMessage(message));
};

export const info = dispatch => ({ info }) => {
  dispatch(publishInfo(info));
};
