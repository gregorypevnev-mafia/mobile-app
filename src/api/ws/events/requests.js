import { requestAdded, friendAdded } from "../../../store/friends/actions";
import { notification } from "../../../store/messages/actions";

export const requestReceived = dispatch => ({ requestId, user, at }) => {
  dispatch(notification(`User ${user.username} has sent you a friend request`));

  dispatch(requestAdded(requestId, user, at));
};

export const requestAccepted = dispatch => ({ requestId, user }) => {
  dispatch(notification(`User ${user.username} has accepted your friend request`));

  dispatch(friendAdded(requestId, user))
};

export const requestRejected = dispatch => ({ user }) => {
  dispatch(notification(`User ${user.username} has rejected your friend request`));
};
