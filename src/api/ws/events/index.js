import { unfriended } from "./friends";
import { requestReceived, requestAccepted, requestRejected } from "./requests";
import { gameCancelled, gameDeleted, gameStarted } from "./games";
import { invitationReceived } from "./invitations";
import { playerAccepted, playerRejected } from "./players";
import { message, info } from "./messages";
import { gameResumed, gamePaused, gameFinished, stageChanged } from "./play";

export default {
  // Friend Events
  "request-received": requestReceived,
  "request-accepted": requestAccepted,
  "request-rejected": requestRejected,

  "unfriended": unfriended,

  // Game Events
  "invitation-received": invitationReceived,

  "game-cancelled": gameCancelled,
  "game-deleted": gameDeleted,
  "game-started": gameStarted,

  "player-accepted": playerAccepted,
  "player-rejected": playerRejected,

  // Play Events
  "game-resumed": gameResumed,
  "game-paused": gamePaused,
  "game-finished": gameFinished,

  "stage-changed": stageChanged,

  // Message Events
  "message": message,
  "info": info,
};
