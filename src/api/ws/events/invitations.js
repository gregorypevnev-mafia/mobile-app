import { invitationAdded } from "../../../store/games/actions";
import { notification } from "../../../store/messages/actions";

export const invitationReceived = dispatch => ({ invitation, host }) => {
  dispatch(notification(`You are invited to a game "${invitation.name}" by ${host.username}`));

  dispatch(invitationAdded(invitation));
};
