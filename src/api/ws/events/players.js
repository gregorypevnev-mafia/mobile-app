import { playerAccepted as accepted } from "../../../store/games/actions";
import { notification } from "../../../store/messages/actions";

export const playerAccepted = dispatch => ({ game, player }) => {
  dispatch(notification(`Player ${player.username} has accepted invitation to "${game.name}"`));

  dispatch(accepted(game.id, player));
};

export const playerRejected = dispatch => ({ game, player }) => {
  dispatch(notification(`Player ${player.username} has rejected invitation to "${game.name}"`));
};
