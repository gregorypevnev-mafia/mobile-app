import { friendDeleted } from "../../../store/friends/actions";
import { notification } from "../../../store/messages/actions";

export const unfriended = dispatch => ({ friendId, user }) => {
  dispatch(notification(`User ${user.username} has unfriended you`));

  dispatch(friendDeleted(friendId));
};
