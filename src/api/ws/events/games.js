import {
  gameCancelled as cancelled,
  gameStarted as started,
  gameDeleted as deleted,
} from "../../../store/games/actions";
import { notification } from "../../../store/messages/actions";

export const gameCancelled = dispatch => ({ game }) => {
  dispatch(notification(`Game "${game.name}" was cancelled`));

  dispatch(cancelled(game.id));
};

export const gameDeleted = dispatch => ({ game }) => {
  dispatch(notification(`Game "${game.name}" was deleted by the host`));

  dispatch(deleted(game.id));
};

export const gameStarted = dispatch => ({ game }) => {
  dispatch(notification(`Game "${game.name}" is now open`));

  dispatch(started(game.id));
};
