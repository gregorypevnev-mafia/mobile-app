import {
  gameResumed as resumed,
  gamePaused as paused,
  stageChanged as changed,
} from "../../../store/play/actions";
import { gameFinished as finished } from "../../../store/common/actions";
import { info as publishInfo } from "../../../store/messages/actions";
import { stageMessage, FINISHED_STATUS } from "../../../utilities/game";

export const gameResumed = (dispatch, state) => ({ }) => {
  const { play: { status } } = state();

  if (status === FINISHED_STATUS) return;

  dispatch(publishInfo({ text: "The game is ready to be played" }));

  dispatch(resumed());
};

export const gamePaused = (dispatch, state) => ({ }) => {
  const { play: { status } } = state();

  if (status === FINISHED_STATUS) return;

  dispatch(publishInfo({ text: "The game has been paused" }));

  dispatch(paused());
};

export const gameFinished = (dispatch, state) => ({ gameId, result, changes, actions }) => {
  const { play: { stage, players } } = state();

  dispatch(finished(gameId, result, changes));

  const stageInfo = stageMessage(stage, stage, players, actions, changes);

  dispatch(publishInfo({ text: [...stageInfo, "The game is over"] }));
};

export const stageChanged = (dispatch, state) => ({ stage: nextStage, changes, actions }) => {
  const { play: { stage: currentStage, players, initialized } } = state();

  dispatch(changed(nextStage, changes));

  // Same stage -> No duplication
  const stageInfo = stageMessage(currentStage, nextStage, players, actions, changes, initialized);

  dispatch(publishInfo({ text: stageInfo }));
};
