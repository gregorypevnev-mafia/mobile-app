import { createClient } from "./client";
import messages from "./messages";
import events from "./events";

const initializeMessages = client => messages =>
  Object.keys(messages).reduce((messengers, messageName) => ({
    ...messengers,
    [messageName]: client.messenger(messages[messageName]),
  }), {});

const initializeEvents = client => (dispatch, state) => events => {
  const eventListeners = Object.keys(events).reduce((listeners, eventName) => ({
    ...listeners,
    [eventName]: events[eventName](dispatch, state),
  }), {});

  client.onMessage((type, payload) => {
    const eventListener = eventListeners[type];

    if (eventListener) eventListener(payload);
  });
};

const createWS = ({ wsURL, ticketURL }, { loadToken }) => {
  const client = createClient({ loadToken }, { wsURL, ticketURL });

  return {
    messages() {
      return initializeMessages(client)(messages);
    },
    events(dispatch, state) {
      return initializeEvents(client)(dispatch, state)(events);
    },
    authenticate() {
      return client.connect();
    },
    unauthenticate() {
      return client.disconnect();
    }
  }
};

export default createWS;
