import { AsyncStorage } from 'react-native'; // Expo-Only
import createStore from "./store";

const TOKEN_KEY = "token";
const USER_KEY = "user";

const createLocalStore = () => ({
  tokens: createStore(AsyncStorage)(TOKEN_KEY),
  users: createStore(AsyncStorage)(USER_KEY),
});

export default createLocalStore;