const createStore = store => key => {
  let value;

  return {
    async load() {
      if (!value) {
        const data = await store.getItem(key);
        value = JSON.parse(data);
      }

      return value;
    },
    // SUPER-IMPORTANT: CAN ONLY STORE STRINGS -> Use JSON for Parsing and Stringifying
    async save(newValue) {
      if (value === newValue) return;

      value = newValue;

      await store.setItem(key, JSON.stringify(value));
    },
    async delete() {
      // Might not be cached -> Read
      value = null;
      await store.removeItem(key);
    }
  }
};

export default createStore;
