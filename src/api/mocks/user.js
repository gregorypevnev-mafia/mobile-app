const USERS = {
  "2c44ef10-74b1-11ea-ae14-0bb7329be6b9": {
    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiMmM0NGVmMTAtNzRiMS0xMWVhLWFlMTQtMGJiNzMyOWJlNmI5IiwidXNlcm5hbWUiOiJHcmVnIiwiaW1hZ2UiOiJkZWZhdWx0LnBuZyIsImF0IjoxNTg3MDI0MDg4NTkzfSwiaWF0IjoxNTg3MDI0MDg4LCJleHAiOjE1ODc2Mjg4ODgsImF1ZCI6IkFQUCIsImlzcyI6IlNZU1RFTSJ9.h-N2jVELdxd3583zTt4f6nYDqMFM5SP7zrvITxgMl0U",
    user: {
      id: "2c44ef10-74b1-11ea-ae14-0bb7329be6b9",
      username: "Greg",
      image: "default.png"
    }
  },
  "a1a28ec0-74b1-11ea-ae14-0bb7329be6b9": {
    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiYTFhMjhlYzAtNzRiMS0xMWVhLWFlMTQtMGJiNzMyOWJlNmI5IiwidXNlcm5hbWUiOiJNYXgiLCJpbWFnZSI6ImRlZmF1bHQucG5nIiwiYXQiOjE1ODcwMjUxNTU3NzR9LCJpYXQiOjE1ODcwMjUxNTUsImV4cCI6MTU4NzYyOTk1NSwiYXVkIjoiQVBQIiwiaXNzIjoiU1lTVEVNIn0.UKa5uDuEGu9FWh6ygu3UvOF-p18Tpd9z8IcWb77bJSk",
    user: {
      id: "a1a28ec0-74b1-11ea-ae14-0bb7329be6b9",
      username: "Max",
      image: "default.png"
    }
  },
  "72fbe370-75cc-11ea-89bb-f15f2bc18cf5": {
    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNzJmYmUzNzAtNzVjYy0xMWVhLTg5YmItZjE1ZjJiYzE4Y2Y1IiwidXNlcm5hbWUiOiJBbGV4IiwiaW1hZ2UiOiJkZWZhdWx0LnBuZyIsImF0IjoxNTg3MDI0MTE1MzA5fSwiaWF0IjoxNTg3MDI0MTE1LCJleHAiOjE1ODc2Mjg5MTUsImF1ZCI6IkFQUCIsImlzcyI6IlNZU1RFTSJ9.VX0JcWcpT2m2JdzGOpGc8PQC7F6WvdyXlkSsHw1ONOw",
    user: {
      id: "72fbe370-75cc-11ea-89bb-f15f2bc18cf5",
      username: "Alex",
      image: "default.png"
    }
  }
};

const DEFAULT_ID = "2c44ef10-74b1-11ea-ae14-0bb7329be6b9";

const getUser = id => USERS[id] || USERS[DEFAULT_ID];

const mockUser = async (tokens, users, ws) => {
  const [_, userId] = location.search.slice(1).split("=");

  const { user, token } = getUser(userId);

  await tokens.save(token);
  await users.save(user);

  await ws.authenticate();

  return user;
};

export default mockUser;
