import * as auth from "./auth";
import user from "./user";

export default {
  auth,
  user,
};