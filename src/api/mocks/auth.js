const MOCK_ID = "1";
const MOCK_TOKEN = "123456";

let call = 2;

const mockUserData = username => ({
  user: {
    id: MOCK_ID,
    username,
  },
  token: MOCK_TOKEN,
});

export const signIn = async ({ username }) => new Promise((res, rej) => {
  call++;

  if (call % 3 == 0) {
    return setTimeout(() => {
      rej({ message: "Invalid login or password" });
    }, 1000);
  }

  setTimeout(() => {
    res(mockUserData(username))
  }, 2000);
});

export const signUp = async ({ username }) => new Promise((res, rej) => {
  call++;

  if (call % 3 == 0) {
    return setTimeout(() => {
      rej({ message: "Username is already taken" });
    }, 1000);
  }

  setTimeout(() => {
    res(mockUserData(username));
  }, 2000);
});

export const signOut = async () => { };