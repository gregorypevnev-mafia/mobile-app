const DEFAULT_CATEGORY = "DEFAULT";

export const defaultState = {
  entities: {},
  list: []
};

export const defaultExtractor = item => String(item.id);

export const defaultCategorizer = _ => DEFAULT_CATEGORY;
