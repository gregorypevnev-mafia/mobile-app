import { defaultExtractor, defaultState } from "./defaults";

export const normalizer = ({
  extractor = defaultExtractor,
} = {}) => {
  const combine = (currentEntities, newItems) => {
    const newEntities = newItems.reduce((entities, item) => {
      const id = extractor(item);

      return {
        ...entities,
        [id]: item,
      }
    }, currentEntities);

    // In order to ensure that there is no duplication (Going to lengths)

    const newList = Array.from(Object.keys(newEntities));

    return {
      entities: newEntities,
      list: newList,
    };
  }

  const put = ({ entities, list } = defaultState, item) => {
    const id = extractor(item);
    const present = !!entities[id];

    const newEntities = {
      ...entities,
      [id]: item,
    };

    return {
      entities: newEntities,
      list: present ? list : [id, ...list],
    }
  };

  const merge = ({ entities } = defaultState, items) => combine(entities, items);

  const replace = ({ } = defaultState, items) => combine({}, items);

  const remove = ({ entities, list } = defaultState, id) => {
    if (!entities[id]) return { entities, list };

    const newList = list.filter(itemId => itemId !== id);

    const newEntities = newList.reduce((newEntities, id) => ({
      ...newEntities,
      [id]: entities[id],
    }), {});

    return {
      list: newList,
      entities: newEntities,
    }
  };

  const update = ({ entities, list } = defaultState, id, updater) => {
    if (!entities[id]) return { entities, list };

    const newEntities = {
      ...entities,
      [id]: updater(entities[id]),
    };

    return {
      entities: newEntities,
      list,
    };
  }

  return {
    put,
    merge,
    replace,
    remove,
    update,
  };
};
