import { defaultCategorizer } from "./defaults";

const addToList = (list, item) => {
  if (!list) return [item];

  return [...list, item];
};

export const denormalizer = ({
  categorizer = defaultCategorizer,
} = {}) => {
  const list = ({ list, entities }) => list.map(id => entities[id]);

  const categories = ({ list, entities }) => {
    return list.reduce((categories, id) => {
      const item = entities[id];
      const category = categorizer(item);

      return {
        ...categories,
        [category]: addToList(categories[category], item),
      }
    }, {});
  }

  return {
    list,
    categories,
  }
}
