import { denormalizer } from "./denormalizer";
import { normalizer } from "./normalizer";
import { defaultState } from "./defaults"

export const createNormalizer = normalizer;

export const createDenormalizer = denormalizer;

export const emptyState = () => Object.assign({}, defaultState);
