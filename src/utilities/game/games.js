import { games as gameUtils } from "../../mafia-game";

export const GAMES_STATUSES = {
  waiting: "WAITING",
  cancelled: "CANCELLED",
  playing: "PLAYING",
  finished: "FINISHED",
};

const DEFAULT_STATUS_MESSAGE = "Unknown";

const STATUS_MESSAGES = {
  [GAMES_STATUSES.waiting]: "Waiting",
  [GAMES_STATUSES.cancelled]: "Cancelled",
  [GAMES_STATUSES.playing]: "Ongoing",
  [GAMES_STATUSES.finished]: "Finished",
};

const mafiaResult = {
  message: "Mafia has won",
  success: false,
};

const townResult = {
  message: "Townspeople have won",
  success: true,
};

export const gameStatusMessage = status => STATUS_MESSAGES[status] || DEFAULT_STATUS_MESSAGE;

const isGameInStatus = checkStatus => status => status === GAMES_STATUSES[checkStatus];

export const isWaiting = isGameInStatus("waiting");
export const isPlaying = isGameInStatus("playing");
export const isCancelled = isGameInStatus("cancelled");
export const isFinished = isGameInStatus("finished");

export const gamesWithResults = games =>
  games.map(({ game: { result, ...data }, ...details }) => ({
    game: {
      ...data,
      result: gameUtils.hasMafiaWon(result) ? mafiaResult : townResult
    },
    ...details,
  }));
