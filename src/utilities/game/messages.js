import { messages } from "../../mafia-game";

export const PLAYER_MESSAGE = "PLAYER_MESSAGE";

export const INFO_MESSAGE = "INFO_MESSAGE";

const time = (at = null) => at || Date.now();

export const playerMessage = ({ by, text, at }) => ({
  type: PLAYER_MESSAGE,
  by,
  text,
  at: time(at)
});

export const infoMessage = ({ text, at }) => ({
  type: INFO_MESSAGE,
  text,
  at: time(at)
});

export const isInfoMessage = ({ type }) => type === INFO_MESSAGE;

export const isPlayerMessage = ({ type }) => type === PLAYER_MESSAGE;

export const stageMessage = (
  currentStage,
  nextStage,
  players,
  actions,
  changes,
  isFirst = false
) => {
  const stageMessages = [];

  // Filtering out initial connection
  if (!isFirst && currentStage.phase !== nextStage.phase)
    stageMessages.push(messages.stageEndMessage(currentStage));

  // Showing what happened (If anything happened at all)
  if (actions && actions.length > 0) {
    const actionMessages = messages.actionMessages(actions, changes, players);

    stageMessages.push(messages.eventsMessage(currentStage));
    stageMessages.push(...actionMessages);
  }

  stageMessages.push(messages.stageStartMessage(nextStage));

  return stageMessages;
};
