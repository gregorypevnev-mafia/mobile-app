// Game
export const RESUMED_STATUS = "RESUMED";
export const PAUSED_STATUS = "PAUSED";
export const FINISHED_STATUS = "FINISHED";

// Players
export const PLAYER_ALIVE = "ALIVE";
export const PLAYER_EXPOSED = "EXPOSED";
export const PLAYER_DEAD = "DEAD";

const PLAYER_STATE_NAMES = {
  [PLAYER_ALIVE]: "Alive",
  [PLAYER_EXPOSED]: "Exposed",
  [PLAYER_DEAD]: "Dead",
};

export const stateName = state => String(PLAYER_STATE_NAMES[state] || "Unknown");
