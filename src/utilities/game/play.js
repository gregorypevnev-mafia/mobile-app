import { players } from "../../mafia-game";
import { RESUMED_STATUS } from "./statuses";

export const isActive = ({
  loaded,
  joined,
  error,
  stage,
  status,
}, player) => {
  // 1. Loading and Joining is complete
  if (!loaded || !joined) return false;

  // 2. No errors
  if (error) return false;

  // 3. The game is resumed 
  if (status !== RESUMED_STATUS) return false;

  // 4. Player is viable to vote (Not dead and has matching role)
  return players.isPlayerActive(stage, player);
  // IMPORTANT: Voting does NOT make you inactive -> Can still view and message
};
