import { actions, roles, games, players } from "../../mafia-game";

export * from "./statuses";
export * from "./messages";
export * from "./games";
export * from "./play";

export const roleName = roles.roleName;

export const phaseName = games.phaseName;

export const phaseDuration = games.phaseDuration;

export const applyChanges = games.applyChanges;

export const votablePlayers = players.votablePlayers;

export const actionInfo = actions.actionForStage;

export const playerInfo = players.playerInfo;

export const resultMessage = result => {
  if (!result) return null;

  if (games.hasMafiaWon(result)) return "Mafia has won";

  return "Townspeople have won"
}
