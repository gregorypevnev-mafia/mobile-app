import moment from "moment";
import { DATE_FORMAT } from "./formats";

export const timeInfo = at => {
  const currentTimestamp = moment();
  const infoTimestamp = moment(at || Date.now());

  if (currentTimestamp.diff(infoTimestamp, "day") < 1)
    return infoTimestamp.from(currentTimestamp);

  return infoTimestamp.format(DATE_FORMAT);
};
