import moment from "moment";
import { DATE_FORMAT, TIME_FORMAT } from "./formats";

export const toDate = at => moment(at || Date.now()).format(DATE_FORMAT);

export const toTime = at => moment(at || Date.now()).format(TIME_FORMAT);
