import {
  GAMES_LOADING,
  GAMES_ERROR,
  GAMES_LOADED,

  INVITATION_ACCEPTING,
  INVITATION_REJECTING,
  INVITATION_ERROR,
  INVITATION_ACCEPTED,
  INVITATION_REJECTED,

  GAME_CREATING,
  GAME_CREATION_ERROR,
  GAME_CREATED,

  GAME_DELETING,
  GAME_DELETION_ERROR,
  GAME_DELETED,

  PLAYER_ACCEPTED,
  GAME_CANCELLED,
  GAME_STARTED,
  INVITATION_ADDED,
} from "./types";
import { GAME_FINISHED } from "../common/types";
import { stateUtils } from "../utilities";
import { createNormalizer, emptyState } from "../../utilities/normalization";
import { GAMES_STATUSES } from "../../utilities/game";

const gameExtractor = game => String(game.id);
const invitationExtractor = invitation => String(invitation.id);

const gameNormalizer = createNormalizer({ extractor: gameExtractor });
const invitationNormalizer = createNormalizer({ extractor: invitationExtractor });

const changeInvitationState = state => (invitations, invitationId) =>
  invitationNormalizer.update(
    invitations,
    invitationId,
    invitation => ({
      ...invitation,
      ...state,
    }),
  );

const acceptingInvitation = changeInvitationState(stateUtils.acceptingState());
const rejectingInvitation = changeInvitationState(stateUtils.rejectingState());

const changeGameStatus = status => (games, gameId, extra = {}) =>
  gameNormalizer.update(games, gameId, game => ({
    ...game,
    status,
    ...extra,
  }));

const cancelledGame = changeGameStatus(GAMES_STATUSES.cancelled);
const playingGame = changeGameStatus(GAMES_STATUSES.playing);
const finishedGame = changeGameStatus(GAMES_STATUSES.finished);

const initialState = {
  loading: false,
  submitting: false,
  error: null,
  submissionError: null,
  invitations: emptyState(),
  games: emptyState(),
  submissionData: null,
};

const gamesReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GAMES_LOADING:
      return { ...state, loading: true };
    case GAME_CREATING:
      return {
        ...state,
        submitting: true,
        submissionData: payload.data
      };
    case GAME_DELETING:
      return {
        ...state,
        games: gameNormalizer.update(
          state.games,
          payload.gameId,
          stateUtils.setDeleting(true)
        ),
      };
    case INVITATION_ACCEPTING:
      return {
        ...state,
        invitations: acceptingInvitation(state.invitations, payload.invitationId),
      };
    case INVITATION_REJECTING:
      return {
        ...state,
        invitations: rejectingInvitation(state.invitations, payload.invitationId),
      };
    case GAMES_ERROR:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    case GAME_CREATION_ERROR:
      return {
        ...state,
        submitting: false,
        submissionError: payload,
      };
    case GAME_DELETION_ERROR:
      return {
        ...state,
        error: payload.error,
        games: gameNormalizer.update(
          state.games,
          payload.gameId,
          stateUtils.setDeleting(false)
        ),
      };
    case INVITATION_ERROR:
      return {
        ...state,
        error: payload.error,
        invitations: invitationNormalizer.update(
          state.invitations,
          payload.invitationId,
          invitation => ({
            ...invitation,
            ...stateUtils.initialState(),
          }),
        )
      };
    case GAMES_LOADED:
      return {
        ...state,
        loading: false,
        error: null,
        games: gameNormalizer.replace(state.games, payload.games),
        invitations: invitationNormalizer.replace(state.invitations, payload.invitations),
      };
    case GAME_CREATED:
      return {
        ...state,
        submitting: false,
        submissionError: null,
        submissionData: null,
        games: gameNormalizer.put(state.games, payload.game),
      };
    case GAME_DELETED:
      return {
        ...state,
        error: null,
        games: gameNormalizer.remove(state.games, payload.gameId),
      };
    case GAME_CANCELLED:
      return {
        ...state,
        invitations: invitationNormalizer.remove(state.invitations, payload.gameId),
        games: cancelledGame(state.games, payload.gameId),
      };
    case GAME_STARTED:
      return {
        ...state,
        games: playingGame(state.games, payload.gameId),
      };
    case GAME_FINISHED:
      return {
        ...state,
        games: finishedGame(state.games, payload.gameId, {
          result: payload.result,
        }),
      };
    case INVITATION_ACCEPTED:
      return {
        ...state,
        error: null,
        invitations: invitationNormalizer.remove(state.invitations, payload.invitationId),
        games: gameNormalizer.put(state.games, payload.game),
      };
    case INVITATION_REJECTED:
      return {
        ...state,
        error: null,
        invitations: invitationNormalizer.remove(state.invitations, payload.invitationId),
      };
    case INVITATION_ADDED:
      return {
        ...state,
        invitations: invitationNormalizer.put(state.invitations, payload.invitation),
      };
    case PLAYER_ACCEPTED:
      return {
        ...state,
        games: gameNormalizer.update(state.games, payload.gameId, game => ({
          ...game,
          // Note: If waiting reaches 0, the game is still NOT started - Preparing
          //  -> Showing "Prepaing" status instead of progress / count
          waiting: game.waiting - 1,
          ready: game.ready + 1,
        })),
      };
    default:
      return state;
  }
};

export default gamesReducer;