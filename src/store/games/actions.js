import {
  GAMES_LOADING,
  GAMES_ERROR,
  GAMES_LOADED,

  INVITATION_ACCEPTING,
  INVITATION_REJECTING,
  INVITATION_ERROR,
  INVITATION_ACCEPTED,
  INVITATION_REJECTED,

  GAME_CREATING,
  GAME_CREATION_ERROR,
  GAME_CREATED,

  GAME_DELETING,
  GAME_DELETION_ERROR,
  GAME_DELETED,

  PLAYER_ACCEPTED,
  GAME_CANCELLED,
  GAME_STARTED,
  GAME_FINISHED,
  INVITATION_ADDED,
} from "./types";
import { notification } from "../messages/actions";

// Helpers

export const invitationError = (invitationId, error) => ({
  type: INVITATION_ERROR,
  payload: {
    invitationId,
    error
  }
});

export const gameDeleted = gameId => ({
  type: GAME_DELETED,
  payload: {
    gameId,
  }
})

// Synchronous

export const loadGames = () => async (dispatch, _, { games }) => {
  try {
    dispatch({ type: GAMES_LOADING });

    const data = await games.load();

    dispatch({ type: GAMES_LOADED, payload: data });
  } catch (e) {
    dispatch({ type: GAMES_ERROR, payload: e.message });
  }
};

export const acceptInvitation = invitationId => async (dispatch, _, { games }) => {
  try {
    dispatch({ type: INVITATION_ACCEPTING, payload: { invitationId } });

    const { game } = await games.acceptInvitation(invitationId);

    dispatch({
      type: INVITATION_ACCEPTED,
      payload: { invitationId, game },
    });

    dispatch(notification("Invitation accepted"));
  } catch (e) {
    dispatch(invitationError(invitationId, e.message));
  }
};

export const rejectInvitation = invitationId => async (dispatch, _, { games }) => {
  try {
    dispatch({ type: INVITATION_REJECTING, payload: { invitationId } });

    await games.rejectInvitation(invitationId);

    dispatch({
      type: INVITATION_REJECTED,
      payload: { invitationId },
    });

    dispatch(notification("Invitation rejected"));
  } catch (e) {
    dispatch(invitationError(invitationId, e.message));
  }
};

export const deleteGame = gameId => async (dispatch, _, { games }) => {
  try {
    dispatch({ type: GAME_DELETING, payload: { gameId } });

    await games.delete(gameId);

    dispatch(gameDeleted(gameId));

    dispatch(notification("Game deleted"));
  } catch (e) {
    dispatch({
      type: GAME_DELETION_ERROR,
      payload: {
        gameId,
        error: e.message,
      }
    })
  }
};

export const createGame = data => async (dispatch, _, { games }) => {
  const { name, players, roles } = data;

  try {
    dispatch({ type: GAME_CREATING, payload: { data } });

    const { game } = await games.create(name, players, roles);

    dispatch({ type: GAME_CREATED, payload: { game } });

    dispatch(notification("Game created"));

    return true;
  } catch (e) {
    dispatch({
      type: GAME_CREATION_ERROR,
      payload: e.message
    });

    return false;
  }
};

// Asynchronous

export const invitationAdded = invitation => ({
  type: INVITATION_ADDED,
  payload: {
    invitation,
  }
});

export const playerAccepted = (gameId, player) => ({
  type: PLAYER_ACCEPTED,
  payload: { gameId, player }
});

export const gameCancelled = gameId => ({
  type: GAME_CANCELLED,
  payload: { gameId }
});

export const gameStarted = gameId => ({
  type: GAME_STARTED,
  payload: { gameId }
});
