import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { combineReducers } from "redux";
import userReducer from "./user/reducer";
import friendsReducer from "./friends/reducer";
import gamesReducer from "./games/reducer";
import playReducer from "./play/reducer";
import messagesReducer from "./messages/reducer";

const rootReducer = () => combineReducers({
  user: userReducer,
  friends: friendsReducer,
  games: gamesReducer,
  play: playReducer,
  messages: messagesReducer,
});

const initialState = currentUser => ({
  user: {
    loading: false,
    error: null,
    user: currentUser,
  }
});

// Parameters: Initial State -> Dependencies
const initializeStore = ({ user }, api) =>
  createStore(
    rootReducer(),
    initialState(user),
    applyMiddleware(thunk.withExtraArgument(api))
  );

export default initializeStore;
