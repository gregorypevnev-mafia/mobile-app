import createREST from "../api/rest";
import createLocalStore from "../api/storage";
import createWS from "../api/ws";
import initializeStore from "./store";
import { unauthenticate } from "./user/actions";
import mocks from "../api/mocks";

// Note: Additional Domain-Logic bindings for WS, Storage, Configurations, etc.

const buildStore = async ({ apiURL, wsURL, ticketURL }) => {
  const { tokens, users } = createLocalStore();

  const loadToken = () => tokens.load();
  const authUtils = { loadToken };

  const { restApi, events: restEvents } = createREST({ url: apiURL }, authUtils);

  const ws = createWS({ wsURL, ticketURL }, authUtils);
  const messages = ws.messages();

  // For testing
  // const user = await mocks.user(tokens, users, ws);
  // window.messages = messages;

  // For production
  const user = await users.load();

  // Connect to WS if already authenticated
  if (user) await ws.authenticate();

  const store = initializeStore({ user }, {
    auth: restApi.auth,
    profile: restApi.profile,
    games: restApi.games,
    friends: restApi.friends,
    dashboard: restApi.dashboard,
    messages,
  });

  restEvents.onAuth(async ({ user, token }) => {
    await tokens.save(token);
    await users.save(user);
    await ws.authenticate();

    // No need to dispatch event - Authentication is performed automatically by the action
  });

  restEvents.onUnauth(async () => {
    await tokens.delete();
    await users.delete();
    await ws.unauthenticate();

    // Could be triggered without a specific action
    // Note: Safe to run since the result is idempotent
    store.dispatch(unauthenticate());
  });

  ws.events(store.dispatch, store.getState);

  return store;
};

export default buildStore;
