import {
  MESSAGE,
  INFO,
  CLEAR_MESSAGES,
  NOTIFICATION,
  CLEAR_NOTIFICATIONS
} from "./types";

// Messages

export const send = (user, text) => (_, __, { messages }) => {
  messages.send(text, user.username || "Unknown");
};

export const clearMessages = () => ({
  type: CLEAR_MESSAGES,
});

export const clearNotifications = () => ({
  type: CLEAR_NOTIFICATIONS,
});

// Events

export const message = ({ by, text, at }) => ({
  type: MESSAGE,
  payload: {
    by,
    text,
    at,
  },
});

export const info = ({ text, at }) => ({
  type: INFO,
  payload: {
    text,
    at,
  },
});

export const notification = text => ({
  type: NOTIFICATION,
  payload: text,
});
