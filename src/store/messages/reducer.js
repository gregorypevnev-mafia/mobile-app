import {
  MESSAGE,
  INFO,
  CLEAR_MESSAGES,
  NOTIFICATION,
  CLEAR_NOTIFICATIONS
} from "./types";
import { playerMessage, infoMessage } from "../../utilities/game";

const initialState = {
  play: [],
  notifications: [],
};

const messagesReducer = (state = initialState, { type, payload }) => {
  // Important: Prepending messages to always show the newest ones
  switch (type) {
    case MESSAGE:
      return {
        ...state,
        play: [playerMessage(payload), ...state.play],
      };
    case INFO:
      return {
        ...state,
        play: [infoMessage(payload), ...state.play],
      };
    case CLEAR_MESSAGES:
      return {
        ...state,
        play: [],
      };
    case NOTIFICATION:
      return {
        ...state,
        notifications: [...state.notifications, payload],
      };
    case CLEAR_NOTIFICATIONS:
      return {
        ...state,
        notifications: [],
      };
    default:
      return state;
  }
};

export default messagesReducer;
