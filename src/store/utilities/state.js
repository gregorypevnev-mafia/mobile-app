export const setDeleting = deleting => data => ({ ...data, deleting });

export const acceptingState = () => ({ accepting: true });
export const rejectingState = () => ({ rejecting: true });
export const initialState = () => ({
  accepting: false,
  rejecting: false,
});
