import {
  SIGN_IN_PROGRESS,
  SIGN_UP_PROGRESS,
  SIGN_IN_ERROR,
  SIGN_UP_ERROR,
  AUTHENTICATION,
  UNAUTHENTICATION,
  PROFILE_LOADING,
  PROFILE_ERROR,
  PROFILE_LOADED
} from "./types";

const initialState = {
  signInProgress: false,
  signInError: null,

  signUpProgress: false,
  signUpError: null,

  loading: false,
  error: null,
  user: null,
};

const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SIGN_IN_PROGRESS:
      return {
        ...state,
        signInProgress: true,
      };
    case SIGN_UP_PROGRESS:
      return {
        ...state,
        signUpProgress: true,
      };
    case SIGN_IN_ERROR:
      return {
        ...state,
        signInProgress: false,
        signInError: payload,
      };
    case SIGN_UP_ERROR:
      return {
        ...state,
        signUpProgress: false,
        signUpError: payload,
      };
    case AUTHENTICATION:
      return {
        ...state,
        signInProgress: false,
        signUpProgress: false,
        signInError: null,
        signUpError: null,
        user: payload,
      };
    case UNAUTHENTICATION:
      return {
        ...state,
        user: null,
      };
    case PROFILE_LOADING:
      return {
        ...state,
        loading: true
      };
    case PROFILE_ERROR:
      return {
        ...state,
        loading: false,
        error: payload
      };
    case PROFILE_LOADED:
      return {
        ...state,
        loading: false,
        error: null,
        user: payload,
      };
    default:
      return state;
  }
};

export default userReducer;
