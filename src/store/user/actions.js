import {
  SIGN_IN_PROGRESS,
  SIGN_UP_PROGRESS,
  SIGN_IN_ERROR,
  SIGN_UP_ERROR,
  AUTHENTICATION,
  UNAUTHENTICATION,
  PROFILE_LOADING,
  PROFILE_ERROR,
  PROFILE_LOADED
} from "./types";

export const authenticate = user => ({
  type: AUTHENTICATION,
  payload: user,
});

export const unauthenticate = () => ({
  type: UNAUTHENTICATION,
});

export const signIn = ({ username, password }) => async (dispatch, _, { auth }) => {
  try {
    dispatch({ type: SIGN_IN_PROGRESS });

    const { user } = await auth.signIn({ username, password });

    dispatch(authenticate(user));
  } catch (e) {
    dispatch({ type: SIGN_IN_ERROR, payload: e.message });
  }
};

export const signUp = userData => async (dispatch, _, { auth }) => {
  try {
    dispatch({ type: SIGN_UP_PROGRESS });

    const { user } = await auth.signUp(userData);

    dispatch(authenticate(user));
  } catch (e) {
    dispatch({ type: SIGN_UP_ERROR, payload: e.message });
  }
};

export const signOut = () => async (dispatch, _, { auth }) => {
  await auth.signOut();

  dispatch(unauthenticate());
};

export const loadProfile = () => async (dispatch, _, { profile }) => {
  try {
    dispatch({ type: PROFILE_LOADING });

    const { user } = await profile.load();

    dispatch({ type: PROFILE_LOADED, payload: user });
  } catch (e) {
    dispatch({ type: PROFILE_ERROR, payload: e.message });
  }
};
