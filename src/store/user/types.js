export const SIGN_IN_PROGRESS = "SIGN_IN_PROGRESS";
export const SIGN_UP_PROGRESS = "SIGN_UP_PROGRESS";

export const SIGN_IN_ERROR = "SIGN_IN_ERROR";
export const SIGN_UP_ERROR = "SIGN_UP_ERROR";

export const AUTHENTICATION = "AUTHENTICATION";

export const UNAUTHENTICATION = "UNAUTHENTICATION";

export const PROFILE_LOADING = "PROFILE_LOADING";
export const PROFILE_ERROR = "PROFILE_ERROR";
export const PROFILE_LOADED = "PROFILE_LOADED";
