import { GAME_FINISHED, RESET_PLAY } from "./types";

export const gameFinished = (gameId, result, changes) => ({
  type: GAME_FINISHED,
  payload: { gameId, result, changes }
});

export const resetPlay = () => ({
  type: RESET_PLAY,
});
