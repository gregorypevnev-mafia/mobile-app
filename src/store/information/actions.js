import { GAMES_LOADING, GAMES_ERROR, GAMES_LOADED } from "../games/types";
import { PROFILE_LOADING, PROFILE_ERROR, PROFILE_LOADED } from "../user/types";
import { FRIENDS_LOADING, FRIENDS_ERROR, FRIENDS_LOADED } from "../friends/types";

export const loadAll = () => async (dispatch, _, { dashboard }) => {
  try {
    dispatch({ type: PROFILE_LOADING });
    dispatch({ type: FRIENDS_LOADING });
    dispatch({ type: GAMES_LOADING });

    const {
      user,
      friends,
      requests,
      invitations,
      games
    } = await dashboard.load();

    dispatch({ type: PROFILE_LOADED, payload: user });
    dispatch({ type: FRIENDS_LOADED, payload: { friends, requests } });
    dispatch({ type: GAMES_LOADED, payload: { games, invitations } });
  } catch (e) {
    dispatch({ type: PROFILE_ERROR, payload: e.message });
    dispatch({ type: FRIENDS_ERROR, payload: e.message });
    dispatch({ type: GAMES_ERROR, payload: e.message });
  }
};