// Synchronous
export const GAME_LOADING = "GAME_LOADING";
export const GAME_ERROR = "GAME_ERROR";
export const GAME_LOADED = "GAME_LOADED";

// Asynchronous
export const GAME_JOINING = "GAME_JOINING";
export const GAME_JOINED = "GAME_JOINED";

export const GAME_LEFT = "GAME_LEFT";

export const GAME_RESUMED = "GAME_RESUMED";
export const GAME_PAUSED = "GAME_PAUSED";

export const VOTED = "VOTED";

export const STAGE_CHANGED = "STAGE_CHANGED";
