import {
  GAME_LOADING,
  GAME_ERROR,
  GAME_LOADED,
  GAME_JOINING,
  GAME_JOINED,
  GAME_RESUMED,
  GAME_PAUSED,
  GAME_LEFT,
  VOTED,
  STAGE_CHANGED,
} from "./types";
import { GAME_FINISHED, RESET_PLAY } from "../common/types";
import {
  applyChanges,
  RESUMED_STATUS,
  PAUSED_STATUS,
  FINISHED_STATUS
} from "../../utilities/game";

const initialState = {
  loaded: false,
  joined: false,

  error: null,

  stage: null,
  players: {},

  status: PAUSED_STATUS,

  voted: false,
  initialized: true,

  result: null,
};

const statusFromGame = ({ resumed }) => resumed ? RESUMED_STATUS : PAUSED_STATUS

const playReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GAME_LOADING:
      return {
        ...state,
        loaded: false,
      };
    case GAME_ERROR:
      return {
        ...state,
        loaded: true,
        error: payload,
      };
    case GAME_LOADED:
      return {
        ...state,
        loaded: true,
        error: null,
        stage: payload.stage,
        // Already normalized -> No need for normalizer
        players: payload.players,
        status: statusFromGame(payload),
      };
    case GAME_JOINING:
      return {
        ...state,
        joined: false,
      };
    case GAME_JOINED:
      return {
        ...state,
        joining: false,
        joined: true,
      };
    case GAME_RESUMED:
      return {
        ...state,
        status: RESUMED_STATUS,
        voted: false, // Resetting ability to vote (The entire stage is reset)
        initialized: true, // Resetting messages and initialization information
      };
    case GAME_PAUSED:
      return {
        ...state,
        status: PAUSED_STATUS,
      };
    case VOTED:
      return {
        ...state,
        voted: true,
      }
    case STAGE_CHANGED:
      return {
        ...state,
        stage: payload.stage,
        players: applyChanges(state.players, payload.changes),
        voted: false, // Resetting ability to vote (For the next stage)
        initialized: false,
      };
    case GAME_FINISHED:
      return {
        ...state,
        status: FINISHED_STATUS,
        result: payload.result,
        players: applyChanges(state.players, payload.changes),
      };
    // Leaving Game -> Resetting everything (For certainty)
    case GAME_LEFT:
    case RESET_PLAY:
      return initialState;
    default:
      return state;
  }
};

export default playReducer;