import {
  GAME_LOADING,
  GAME_ERROR,
  GAME_LOADED,
  GAME_JOINING,
  GAME_JOINED,
  GAME_LEFT,
  GAME_RESUMED,
  GAME_PAUSED,
  GAME_FINISHED,
  VOTED,
  STAGE_CHANGED,
} from "./types";

// Synchronous

export const loadGame = gameId => async (dispatch, _, { games }) => {
  try {
    dispatch({ type: GAME_LOADING });

    const { game } = await games.loadDetails(gameId);

    dispatch({
      type: GAME_LOADED,
      payload: game
    })
  } catch (e) {
    dispatch({
      type: GAME_ERROR,
      payload: e.message
    })
  }
};

// Messages

export const joinGame = (username, gameId) => async (dispatch, _, { messages }) => {
  dispatch({ type: GAME_JOINING });

  await messages.joinGame(gameId, username);

  dispatch({ type: GAME_JOINED });
};

export const leaveGame = () => async (dispatch, _, { messages }) => {
  await messages.leaveGame();

  dispatch({ type: GAME_LEFT });
};

export const vote = target => async (dispatch, __, { messages }) => {
  dispatch({ type: VOTED });

  await messages.vote(target);
};

// Events

export const gameResumed = () => ({
  type: GAME_RESUMED
});

export const gamePaused = () => ({
  type: GAME_PAUSED
});

export const stageChanged = (stage, changes) => ({
  type: STAGE_CHANGED,
  payload: {
    stage,
    changes: changes || {},
  }
});
