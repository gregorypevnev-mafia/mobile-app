import {
  FRIENDS_LOADING,
  FRIENDS_ERROR,
  FRIENDS_LOADED,

  REQUEST_ACCEPTING,
  REQUEST_REJECTING,
  REQUEST_ERROR,
  REQUEST_ACCEPTED,
  REQUEST_REJECTED,

  FRIEND_REQUESTING,
  FRIEND_REQUEST_ERROR,
  FRIEND_REQUESTED,

  FRIEND_DELETING,
  FRIEND_DELETION_ERROR,
  FRIEND_DELETED,
  FRIEND_ADDED,

  REQUEST_ADDED,
  REQUEST_DELETED
} from "./types";
import { stateUtils } from "../utilities";
import { createNormalizer, emptyState } from "../../utilities/normalization";

const friendExtractor = friend => String(friend.friendId);
const requestExtractor = request => String(request.requestId);

const friendNormalizer = createNormalizer({ extractor: friendExtractor });
const requestNormalizer = createNormalizer({ extractor: requestExtractor });

const changeRequestState = state => (requests, requestId) =>
  requestNormalizer.update(
    requests,
    requestId,
    request => ({
      ...request,
      ...state,
    }));

const acceptingRequest = changeRequestState(stateUtils.acceptingState());
const rejectingRequest = changeRequestState(stateUtils.rejectingState());

const initialState = {
  loading: false,
  submitting: false,
  error: null,
  submissionError: null,
  requests: emptyState(),
  friends: emptyState(),
  submissionData: null,
};

const friendsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FRIENDS_LOADING:
      return { ...state, loading: true };
    case FRIEND_REQUESTING:
      return {
        ...state,
        submitting: true,
        submissionData: payload.data,
      };
    case FRIEND_DELETING:
      return {
        ...state,
        friends: friendNormalizer.update(
          state.friends,
          payload.friendId,
          stateUtils.setDeleting(true)
        ),
      };
    case REQUEST_ACCEPTING:
      return {
        ...state,
        requests: acceptingRequest(state.requests, payload.requestId)
      };
    case REQUEST_REJECTING:
      return {
        ...state,
        requests: rejectingRequest(state.requests, payload.requestId)
      };
    case FRIENDS_ERROR:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    case FRIEND_REQUEST_ERROR:
      return {
        ...state,
        submitting: false,
        submissionError: payload
      };
    case FRIEND_DELETION_ERROR:
      return {
        ...state,
        error: payload.error,
        friends: friendNormalizer.update(
          state.friends,
          payload.friendId,
          stateUtils.setDeleting(false)
        ),
      };
    case REQUEST_ERROR:
      return {
        ...state,
        error: payload.error,
        requests: requestNormalizer.update(
          state.requests,
          payload.requestId,
          request => ({
            ...request,
            ...stateUtils.initialState(),
          })),
      };
    case FRIENDS_LOADED:
      return {
        ...state,
        loading: false,
        error: null,
        friends: friendNormalizer.replace(state.friends, payload.friends),
        requests: requestNormalizer.replace(state.requests, payload.requests),
      };
    case FRIEND_REQUESTED:
      return {
        ...state,
        submitting: false,
        submissionError: null,
        submissionData: null,
      };
    case FRIEND_DELETED:
      return {
        ...state,
        error: null,
        friends: friendNormalizer.remove(state.friends, payload.friendId)
      };
    case FRIEND_ADDED:
      return {
        ...state,
        friends: friendNormalizer.put(state.friends, payload.friend),
      };
    case REQUEST_ACCEPTED:
      return {
        ...state,
        error: null,
        requests: requestNormalizer.remove(state.requests, payload.requestId),
        friends: friendNormalizer.put(state.friends, payload.friend),
      };
    case REQUEST_REJECTED:
      return {
        ...state,
        error: null,
        requests: requestNormalizer.remove(state.requests, payload.requestId),
      };
    case REQUEST_ADDED:
      return {
        ...state,
        requests: requestNormalizer.put(state.requests, payload.request),
      };
    case REQUEST_DELETED:
      return {
        ...state,
        requests: requestNormalizer.remove(state.requests, payload.requestId)
      };
    default:
      return state;
  }
};

export default friendsReducer;