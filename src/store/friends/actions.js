import {
  FRIENDS_LOADING,
  FRIENDS_ERROR,
  FRIENDS_LOADED,

  REQUEST_ACCEPTING,
  REQUEST_REJECTING,
  REQUEST_ERROR,
  REQUEST_ACCEPTED,
  REQUEST_REJECTED,

  FRIEND_REQUESTING,
  FRIEND_REQUEST_ERROR,
  FRIEND_REQUESTED,

  FRIEND_DELETING,
  FRIEND_DELETION_ERROR,
  FRIEND_DELETED,
  FRIEND_ADDED,

  REQUEST_ADDED,
  REQUEST_DELETED
} from "./types";
import { notification } from "../messages/actions";

// Creators

export const requestError = (requestId, error) =>
  ({
    type: REQUEST_ERROR,
    payload: {
      error,
      requestId
    }
  });

export const friendDeleted = friendId =>
  ({
    type: FRIEND_DELETED,
    payload: { friendId }
  });

// Synchronous

export const loadFriends = () => async (dispatch, _, { friends }) => {
  try {
    dispatch({ type: FRIENDS_LOADING });

    const friendsData = await friends.load();

    dispatch({ type: FRIENDS_LOADED, payload: friendsData });
  } catch (e) {
    dispatch({ type: FRIENDS_ERROR, payload: e.message });
  }
};

export const acceptRequest = requestId => async (dispatch, _, { friends }) => {
  try {
    dispatch({ type: REQUEST_ACCEPTING, payload: { requestId } });

    const { friend } = await friends.acceptRequest(requestId);

    dispatch({
      type: REQUEST_ACCEPTED,
      payload: { friend, requestId }
    });

    dispatch(notification("Friendship request accepted"));
  } catch (e) {
    dispatch(requestError(requestId, e.message));
  }
};

export const rejectRequest = requestId => async (dispatch, _, { friends }) => {
  try {
    dispatch({ type: REQUEST_REJECTING, payload: { requestId } });

    await friends.rejectRequest(requestId);

    dispatch({
      type: REQUEST_REJECTED,
      payload: { requestId }
    });

    dispatch(notification("Friendship request rejected"));
  } catch (e) {
    dispatch(requestError(requestId, e.message));
  }
};

export const requestFriend = friendData => async (dispatch, _, { friends }) => {
  try {
    dispatch({ type: FRIEND_REQUESTING, payload: { data: friendData } });

    await friends.request(friendData);

    dispatch({ type: FRIEND_REQUESTED });

    dispatch(notification("Friendship request sent"));

    return true;
  } catch (e) {
    dispatch({ type: FRIEND_REQUEST_ERROR, payload: e.message });

    return false;
  }
};

export const deleteFriend = friendId => async (dispatch, _, { friends }) => {
  try {
    dispatch({ type: FRIEND_DELETING, payload: { friendId } });

    await friends.delete(friendId);

    dispatch(friendDeleted(friendId));

    dispatch(notification("Unfriended"));
  } catch (e) {
    dispatch({
      type: FRIEND_DELETION_ERROR,
      payload: {
        friendId,
        error: e.message,
      }
    });
  }
};

// Asynchronous

export const friendAdded = (friendId, user) =>
  ({
    type: FRIEND_ADDED,
    payload: {
      friend: {
        friendId,
        user,
      }
    },
  });

export const requestAdded = (requestId, user, at) =>
  ({
    type: REQUEST_ADDED,
    payload: {
      request: {
        requestId,
        user,
        at,
      }
    }
  });

export const requestDeleted = requestId =>
  ({
    type: REQUEST_DELETED,
    payload: {
      requestId,
    }
  });
