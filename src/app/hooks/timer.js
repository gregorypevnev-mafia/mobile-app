import { useState, useEffect } from "react";

const useTimer = (initialTime, name = "", active = true) => {
  const [time, setTime] = useState(initialTime);

  // Do NOT use Interval - Timeout after every change -> More consistent ad reliable
  useEffect(() => {
    const interval = setTimeout(() => {
      if (active) setTime(Math.max(time - 1, 0));
    }, 1000);

    return () => clearInterval(interval);
  }, [time, active]);

  useEffect(() => {
    setTime(initialTime);
  }, [initialTime, name, active]);

  return time;
};

export default useTimer;