import { useState, useEffect } from "react";

// Messages - List of mesages for reading and showing
// OnMessage - Showing the next mesage(s)
// OnClear - Clearing all current messages (On initialization) -> Avoiding duplication
const useMessage = (messages, onMessage, onClear) => {
  const [cleared, setCleared] = useState(false);
  const [index, setIndex] = useState(0);

  useEffect(() => {
    setCleared(true);


    if (onClear) onClear();
  }, []);

  useEffect(() => {
    const message = messages[index];

    if (!message || !cleared) return;

    if (onMessage) onMessage(message);
  }, [index, messages]);

  const nextMessage = () => setIndex(index + 1);

  return [{ cleared, index }, { nextMessage }];
};

export default useMessage;
