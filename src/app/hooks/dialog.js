import { useState } from "react";

const useDialog = () => {
  const [isOpen, setOpen] = useState(false);

  const openDialog = () => setOpen(true);
  const closeDialog = () => setOpen(false);

  const setters = { openDialog, closeDialog };

  return [isOpen, setters];
};

export default useDialog;
