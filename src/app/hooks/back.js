import { useEffect } from "react";
import { Alert, BackHandler } from "react-native";

const useBack = (message, isAllowed, onBack) => {
  const back = () => {
    if (isAllowed()) return false; // Not handling (Letting go back in case of an error)

    Alert.alert("Are you sure?", message, [
      {
        text: "Cancel",
        onPress: () => null,
        style: "cancel"
      },
      { text: "Back", onPress: onBack }
    ]);

    return true;
  };

  useEffect(() => {
    const backHandler = BackHandler.addEventListener("hardwareBackPress", back);

    return () => backHandler.remove();
  }, []);
};

export default useBack;
