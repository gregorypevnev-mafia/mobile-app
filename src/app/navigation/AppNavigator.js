import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { connect } from "react-redux";
import AuthNavigator from "./auth/AuthNavigator";
import MainNavigator from "./main/MainNavigator";

const AppNavigator = ({ authed }) => (
  <NavigationContainer>
    {
      authed ? (
        <MainNavigator />
      ) : (
          <AuthNavigator />
        )
    }
  </NavigationContainer>
)

const mapStateToProps = ({ user: { user } }) => ({
  authed: !!user,
});

export default connect(mapStateToProps)(AppNavigator);