import React from "react";
import { createStackNavigator } from '@react-navigation/stack';
import SignInScreen from "../../screens/auth/signin/SignInScreen";
import SignUpScreen from "../../screens/auth/signup/SignUpScreen";

const AuthStack = createStackNavigator();

const AuthNavigator = () => (
  <AuthStack.Navigator
    initialRouteName="Sign In"
    headerMode="none"
  >
    <AuthStack.Screen name="Sign In" component={SignInScreen} />
    <AuthStack.Screen name="Sign Up" component={SignUpScreen} />
  </AuthStack.Navigator>
)

export default AuthNavigator;
