import React, { useEffect, useRef } from "react";
import { View, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { createStackNavigator } from '@react-navigation/stack';
import Toast from "react-native-easy-toast";
import InformationNavigator from "../information/InformationNavigator";
import GameScreen from "../../screens/game/GameScreen";
import useMessage from "../../hooks/messages";
import { mapStateToProps, mapDispatchToProps } from "./container";

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

const MainStack = createStackNavigator();

const MainNavigator = ({ notifications, clearNotifications, resetPlay }) => {
  const ref = useRef(null);

  const [_, { nextMessage }] = useMessage(
    notifications, // Messages / Notifications to read
    notification => ref.current.show(notification, 1000, nextMessage), // Showing a new message
    clearNotifications, // Clearinng
  );

  // Cleaning up before a possible prior game (Which exited unexpectedbly)
  useEffect(() => {
    resetPlay();
  }, []);

  return (
    <View style={styles.container}>
      <MainStack.Navigator
        headerMode="none"
        initialRouteName="Information"
      >
        <MainStack.Screen
          name="Information"
          component={InformationNavigator}
        />
        <MainStack.Screen
          name="Game"
          component={GameScreen}
        // initialParams={{ gameId: "61a916f0-7a82-11ea-bfe1-692918df6109" }}
        />
      </MainStack.Navigator>
      <Toast ref={ref} />
    </View>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(MainNavigator);
