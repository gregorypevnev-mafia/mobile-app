import { bindActionCreators } from "redux";
import { clearNotifications } from "../../../../store/messages/actions";
import { resetPlay } from "../../../../store/common/actions";

const mapActions = dispatch =>
  bindActionCreators({
    clearNotifications,
    resetPlay,
  }, dispatch);

export default mapActions;
