import mapActions from "./actions";
import mapData from "./data";

export const mapStateToProps = mapData;
export const mapDispatchToProps = mapActions;
