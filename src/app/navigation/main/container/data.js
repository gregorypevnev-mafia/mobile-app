const mapData = ({ messages: { notifications } }) => ({
  notifications,
});

export default mapData;
