import React from "react";
import Layout from "../../../components/layout/Layout";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { signOut } from "../../../../store/user/actions";

const mapStateToProps = ({ user: { user } }) => ({ user });

const mapDispatchToProps = dispatch => bindActionCreators({ signOut }, dispatch);

const withInformationNavigation = navigation => Component => {
  const playGame = gameId => navigation.push("Game", { gameId });

  const informationComponent = ({ user, signOut, ...props }) => (
    <Layout user={user.username} signOut={signOut}>
      <Component {...props} playGame={playGame} />
    </Layout>
  );

  return connect(mapStateToProps, mapDispatchToProps)(informationComponent);
}

export default withInformationNavigation;
