import { bindActionCreators } from "redux";
import { loadAll } from "../../../../store/information/actions";

const mapActions = dispatch =>
  bindActionCreators({ loadAll }, dispatch);

export default mapActions;
