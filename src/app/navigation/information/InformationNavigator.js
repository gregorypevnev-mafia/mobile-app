import React, { useEffect } from "react";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from "@expo/vector-icons";
import { connect } from "react-redux";
import withInformationNavigation from "./hoc/withInformationNavigation";
import HomeScreen from "../../screens/information/home/HomeScreen";
import FriendsScreen from "../../screens/information/friends/FriendsScreen";
import GamesScreen from "../../screens/information/games/GamesScreen";
import { mapStateToProps, mapDispatchToProps } from "./container";

const ICONS = {
  "Home": "ios-home",
  "Friends": "ios-people",
  "Games": "ios-play-circle",
};

const Tab = createBottomTabNavigator();

const InformationNavigator = ({ navigation, loadAll }) => {
  const withInformation = withInformationNavigation(navigation);

  useEffect(() => {
    loadAll();
  }, []);

  return (
    <Tab.Navigator
      // Customzing outlook
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, size }) => {
          return <Ionicons name={ICONS[route.name]} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: "#0077ff",
        inactiveTintColor: "#ccc",
      }}
      initialRouteName="Home"
    >
      <Tab.Screen name="Home" component={withInformation(HomeScreen)} />
      <Tab.Screen name="Friends" component={withInformation(FriendsScreen)} />
      <Tab.Screen name="Games" component={withInformation(GamesScreen)} />
    </Tab.Navigator>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(InformationNavigator);
