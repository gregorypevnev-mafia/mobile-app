import React from "react";
import { View, StyleSheet } from "react-native";
import Heading from "./components/Heading";
import NameForm from "./components/NameForm";
import RolesForm from "./components/RolesForm";
import PlayersForm from "./components/PlayersForm";
import Controls from "./components/Controls";
import Error from "../../common/Error";
import useGame from "./hooks";
import { formatOutput } from "./utils/format";
import Loading from "../../common/Loading";

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  head: {
    flexGrow: 0,
    flexShrink: 0,
    marginBottom: 20
  },
  form: {
    flex: 1,
    flexGrow: 1,
    flexShrink: 1,
    marginBottom: 15,
    flexBasis: 200,
    borderBottomColor: "#dedede",
    borderBottomWidth: 1
  },
  footer: {
    flexGrow: 0,
    flexShrink: 0
  }
});

const GameForm = ({
  submitting,
  error,
  defaults,
  players,
  onCancel,
  onSubmit,
}) => {
  const [state, setters] = useGame(defaults || {}, { players: players.list });

  const data = {
    name: state.name,
    roles: state.roles,
    players: state.players
  };

  return (
    <View style={styles.container}>
      <View style={styles.head}>
        <Heading step={state.step} />
      </View>
      <View style={styles.form}>
        {state.step === 0 && (
          <NameForm name={state.name} onChange={setters.changeName} />
        )}
        {state.step === 1 && (
          <RolesForm roles={state.roles} onRoleConfigured={setters.setRole} />
        )}
        {state.step === 2 && (
          <PlayersForm
            loading={players.loading}
            error={players.error}
            players={state.players}
            count={state.playersCount}
            space={state.rolesCount}
            onPlayerSelected={setters.selectPlayer}
          />
        )}
      </View>
      <View style={styles.footer}>
        <Controls
          isStart={state.isStart}
          isEnd={state.isEnd}
          isValid={state.isValid}
          isDisabled={submitting}
          onNext={() => (
            (state.isEnd && state.isValid) ?
              onSubmit(formatOutput(data)) :
              setters.nextStep()
          )}
          onBack={() => (
            state.isStart ?
              onCancel() :
              setters.prevStep()
          )}
        />
        {submitting && <Loading>Creating a new game...</Loading>}
        {error && <Error>{error}</Error>}
      </View>
    </View>
  );
};

export default GameForm;
