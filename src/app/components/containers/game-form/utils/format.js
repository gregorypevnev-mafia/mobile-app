import { roles as mafiaRoles } from "../../../../../mafia-game";

const formatRoles = roles => mafiaRoles.rolesFromList(roles);

const formatPlayers = players =>
  players
    .filter(({ active }) => active)
    .map(({ id }) => String(id))

export const formatOutput = ({
  name,
  roles,
  players
}) => ({
  name,
  roles: formatRoles(roles),
  players: formatPlayers(players),
});
