import { roles as mafiaRoles } from "../../../../../mafia-game";

const validateName = ({ name }) =>
  name && String(name).length > 0;

const validateRoles = ({ roles }) =>
  mafiaRoles.validateRoles(mafiaRoles.rolesFromList(roles))

const validatePlayers = ({ playersCount, rolesCount }) =>
  // Taking current user into account
  (Number(playersCount) + 1) === Number(rolesCount);

const VALIDATORS = {
  0: validateName,
  1: validateRoles,
  2: validatePlayers,
};

const validate = ({ step, ...data }) =>
  !!(VALIDATORS[step] && VALIDATORS[step](data));

export default validate;
