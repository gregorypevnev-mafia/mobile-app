import React from "react";
import { Text, StyleSheet } from "react-native";

const TITLES = {
  0: "Enter name of the game",
  1: "Adjust the roles",
  2: "Select players"
};

const DEFAULT_TITLE = "???";

const styles = StyleSheet.create({
  heading: {
    fontSize: 18,
    fontWeight: "bold",
  }
});

const Heading = ({ step }) => (
  <Text style={styles.heading}>
    {`Step ${step + 1}. ${TITLES[step] || DEFAULT_TITLE}`}
  </Text>
);

export default Heading;
