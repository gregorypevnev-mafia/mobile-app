import React from "react";
import { View } from "react-native";
import Information from "../../../common/Information";
import Status from "./Status";
import PlayersList from "./PlayersList";

const PlayersForm = ({
  loading,
  error,
  players,
  count,
  space,
  onPlayerSelected
}) => (
    <View>
      <PlayersList players={players} onSelected={onPlayerSelected} />
      <Status count={count} required={space - 1} />
      <Information
        loadingText="Loading possile players..."
        loading={loading}
        error={error ? "Could not load friends" : null}
      />
    </View>
  );

export default PlayersForm;
