import React from "react";
import { View, Text, ScrollView, Slider, StyleSheet } from "react-native";
import RoleInput from "./RoleInput";
import { roleName } from "../../../../../utilities/game";

const styles = StyleSheet.create({
  title: {
    fontSize: 14,
    fontWeight: "bold",
    marginBottom: 5
  },
  slider: {
    marginBottom: 10
  }
});

const roleTitle = ({ name, count }) => `${roleName(name, true)} (${count})`;

const RolesForm = ({ roles, onRoleConfigured }) => (
  <ScrollView>
    {/* Only rendered once -> No need for FlatList */}
    {roles.map(role => (
      <View style={styles.slider} key={role.name}>
        <Text style={styles.title}>{roleTitle(role)}</Text>
        <RoleInput {...role} onChanged={count => onRoleConfigured(role.name, count)} />
      </View>
    ))}
  </ScrollView>
);

export default RolesForm;