import React from "react";
import { Text } from "react-native";

const Status = ({ count, required }) => (
  <Text>{`Status: ${count}/${required}`}</Text>
);

export default Status;
