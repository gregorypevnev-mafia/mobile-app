import React from "react";
import Input from "../../../common/Input";

const NameForm = ({ name, onChange }) => (
  <Input
    placeholder="Name of the game"
    value={name}
    onInput={onChange}
  />
);

export default NameForm;
