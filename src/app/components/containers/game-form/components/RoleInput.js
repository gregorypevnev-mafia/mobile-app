import React from "react";
import { View, Text, StyleSheet, Slider, Platform } from "react-native";
import WebSlider from "react-native-slider";

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center"
  },
  slider: {
    width: 200,
    height: 40,
    marginHorizontal: 15
  }
})

const RoleInput = ({ min, count, max, onChanged }) => (
  <View style={styles.container}>
    <Text>{min}</Text>
    {Platform.OS === "web" ? (
      <WebSlider
        style={styles.slider}
        minimumValue={min}
        maximumValue={max}
        value={count}
        step={1}
        onValueChange={newCount => onChanged(newCount)}
      />
    ) : (
        <Slider
          style={styles.slider}
          minimumValue={min}
          maximumValue={max}
          value={count}
          step={1}
          onValueChange={newCount => onChanged(newCount)}
        />
      )}
    <Text>{max}</Text>
  </View>
);

export default RoleInput;