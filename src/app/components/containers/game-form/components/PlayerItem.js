import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Button from "../../../common/Button";

const styles = StyleSheet.create({
  playerItem: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 15,
    backgroundColor: "#ffffff",
    borderBottomColor: "#cdcdcd",
    borderBottomWidth: 1
  }
});

const PlayerItem = ({ user, active, onSelected }) => (
  <View style={styles.playerItem}>
    <Text>{user.username}</Text>
    <Button
      onPress={onSelected}
      danger={active}
    >
      {active ? "Unselect" : "Select"}
    </Button>
  </View>
);

export default PlayerItem;
