import React from "react";
import { View, StyleSheet } from "react-native";
import Button from "../../../common/Button";

const styles = StyleSheet.create({
  controls: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10
  }
});

const Controls = ({
  isEnd,
  isStart,
  isValid,
  isDisabled,
  onNext,
  onBack
}) => (
    <View style={styles.controls}>
      <Button onPress={onBack} disabled={isDisabled} danger>
        {isStart ? "Cancel" : "Back"}
      </Button>
      <Button onPress={onNext} disabled={!isValid || isDisabled}>
        {isEnd ? "Finish" : "Next"}
      </Button>
    </View>
  );

export default Controls;
