import React from "react";
import { View, Text, FlatList, StyleSheet } from "react-native";
import PlayerItem from "./PlayerItem";
import Placeholder from "../../../common/Placeholder";

const styles = StyleSheet.create({
  container: {
    height: 150,
    marginBottom: 10
  },
  list: {
    flex: 1
  }
});

const isEmpty = players => !players || players.length === 0;

const PlayersList = ({ players, onSelected }) => (
  <View style={styles.container}>
    {
      isEmpty(players) ? (
        <Placeholder>
          <Text>No friends to select players from</Text>
        </Placeholder>
      ) : (
          <FlatList
            style={styles.list}
            data={players}
            keyExtractor={player => player.id}
            renderItem={({ item }) => (
              <PlayerItem
                user={item.user}
                active={item.active}
                onSelected={() => onSelected(item.id)}
              />
            )}
          />
        )
    }
  </View>
);

export default PlayersList;
