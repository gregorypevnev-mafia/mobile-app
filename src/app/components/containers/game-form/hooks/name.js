import { useState } from "react";

const MAX_LENGTH = 50;

const useName = (initialName = "") => {
  const [name, setName] = useState(initialName);

  const changeName = newName =>
    String(newName || "").length < MAX_LENGTH && setName(newName);

  return [name, changeName];
};

export default useName;
