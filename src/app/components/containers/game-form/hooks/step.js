import { useState } from "react";

const START_STEP = 0;

const END_STEP = 2;

const formatStep = step => ({
  step,
  isStart: step === START_STEP,
  isEnd: step === END_STEP,
})

const useStep = () => {
  const [step, setStep] = useState(0);

  const nextStep = () => setStep(Math.min(step + 1, END_STEP));
  const prevStep = () => setStep(Math.max(step - 1, START_STEP));

  const state = formatStep(step);
  const setters = { nextStep, prevStep };

  return [state, setters];
};

export default useStep;
