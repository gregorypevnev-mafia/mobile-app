import useStep from "./step";
import useName from "./name";
import useRoles from "./roles";
import usePlayers from "./players";
import validate from "../utils/validators";

const useGame = (defaults, { players: availablePlayers }) => {
  const [{ step, isStart, isEnd }, { nextStep, prevStep }] = useStep();
  const [name, changeName] = useName(defaults.name || "");
  const [{ roles, rolesCount }, setRole] = useRoles(defaults.roles || null);
  const [{ players, playersCount }, selectPlayer] = usePlayers(availablePlayers, defaults.players || []);

  const data = {
    step,
    isStart,
    isEnd,
    name,
    roles,
    rolesCount,
    players,
    playersCount,
  };

  const state = {
    ...data,
    isValid: validate(data),
  };

  const setters = {
    nextStep,
    prevStep,
    changeName,
    setRole,
    selectPlayer,
  };

  return [state, setters];
};

export default useGame;
