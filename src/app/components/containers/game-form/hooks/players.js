import { useState } from "react";

const toMap = ids => ids.reduce((map, id) => ({
  ...map,
  [id]: true
}), {});

const toTable = (players, selected) => {
  const selectedPlayers = toMap(selected || {});

  return players.reduce((states, { id }) => ({
    ...states,
    [id]: !!selectedPlayers[id],
  }), {});
}

const toList = (players, playersStates) =>
  players.map(player => ({
    id: player.id,
    user: player.user,
    active: !!playersStates[player.id]
  }));

const countPlayers = players =>
  Object.keys(players).filter(player => players[player]).length;

const usePlayers = (availablePlayers, selected = []) => {
  const [players, setPlayers] = useState(toTable(availablePlayers, selected));

  const selectPlayer = player => setPlayers({
    ...players,
    [player]: !players[player],
  });

  const state = {
    players: toList(availablePlayers, players),
    playersCount: countPlayers(players),
  };

  return [state, selectPlayer];
};

export default usePlayers;