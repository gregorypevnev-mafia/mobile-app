import { useState } from "react";
import { roles as mafiaRoles } from "../../../../../mafia-game";

const useRoles = (initialRoles = null) => {
  const [roles, setRoles] = useState(initialRoles || mafiaRoles.initializeRoles());

  const setRole = (role, count) => setRoles({
    ...roles,
    [role]: count,
  });

  const state = {
    roles: mafiaRoles.rolesToList(roles),
    rolesCount: mafiaRoles.countRoles(roles),
  };

  return [state, setRole];
};

export default useRoles;
