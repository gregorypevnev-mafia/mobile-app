import React from "react";
import { View, StyleSheet } from "react-native";
import Profile from "./profile/Profile";
import Details from "./details/Details";
import Chat from "./chat/Chat";
import GameContent from "./GameContent";

const styles = StyleSheet.create({
  view: {
    flex: 1,
    flexDirection: "column",
    alignItems: "stretch",
    justifyContent: "flex-start",
    position: "relative"
  },
  profileSection: {
    flexGrow: 0,
    flexShrink: 0,
    flexBasis: 75
  },
  detailsSection: {
    flexGrow: 0,
    flexShrink: 0,
    flexBasis: 50
  },
  contentSection: {
    flex: 1,
  },
  chatSection: {
    flexBasis: 200,
  }
});

const Game = ({
  isActive,
  isWaiting,
  isFinished,
  hasVoted,
  stage: {
    step,
    phase,
  },
  player,
  players,
  messages,
  time,
  action,
  result,
  onVote,
  onSkip,
  onMessage,
  onExit,
}) => (
    <View style={styles.view}>
      <View style={styles.profileSection}>
        <Profile profile={player} />
      </View>
      <View style={styles.detailsSection}>
        <Details
          step={step}
          phase={phase}
          time={time}
          finished={isFinished}
        />
      </View>
      <View style={styles.contentSection}>
        <GameContent
          isActive={isActive}
          isWaiting={isWaiting}
          isFinished={isFinished}
          hasVoted={hasVoted}
          players={players}
          action={action || { skippable: false, title: null }}
          result={result}
          onSkip={onSkip}
          onVote={onVote}
          onExit={onExit}
        />
      </View>
      <View style={styles.chatSection}>
        <Chat
          messages={messages}
          onMessage={onMessage}
          isActive={isActive}
        />
      </View>
    </View>
  );

export default Game;
