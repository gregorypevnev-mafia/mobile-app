import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Button from "../../../common/Button";
import Image from "../../../common/Image";
import resources from "../../../../resources";
import { playerInfo } from "../../../../../utilities/game";

const styles = StyleSheet.create({
  item: {
    width: 250,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: "#ffffff",
    ...resources.shadow,
  },
  nameText: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10
  },
  infoContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10,
    alignSelf: "stretch"
  },
  roleText: {
    fontSize: 16,
  },
  statusText: {
    fontSize: 16,
    fontStyle: "italic"
  },
  placeholderContainer: {
    height: 50,
    justifyContent: "center"
  }
});

const activeDisplay = styles.item;
const inactiveDisplay = StyleSheet.compose(styles.item, { opacity: 0.4 });

const MenuItem = ({
  username,
  image,
  role,
  status,
  votable,
  action,

  isActive,
  hasVoted,

  onAction,
}) => {
  // ANy display and activity can ONLY be performed by an active member
  // Can perform action and Cannot vote -> Same role
  const sameRole = isActive && !votable && action;
  // Can perform actoin and Can vote -> Different role OR Trial-Phase
  const otherRole = isActive && votable && action;
  const info = playerInfo({ role, status }, sameRole);

  return (
    <View style={otherRole ? activeDisplay : inactiveDisplay}>
      <View style={styles.infoContainer}>
        <Text style={styles.nameText}>{username}</Text>
        <Image uri={image} width="50" height="50" />
      </View>

      <View style={styles.infoContainer}>
        <Text style={styles.roleText}>{info.role}</Text>
        <Text style={styles.statusText}>{info.status}</Text>
      </View>

      <View style={styles.placeholderContainer}>
        {otherRole && (
          <Button
            disabled={hasVoted}
            onPress={onAction}
          >
            {action}
          </Button>
        )}
      </View>
    </View>
  );
}

export default MenuItem;