import React from "react";
import { View, Text, FlatList, StyleSheet } from "react-native";
import MenuItem from "./MenuItem";
import Button from "../../../common/Button";

const styles = StyleSheet.create({
  menu: {
    alignItems: "stretch"
  },
  item: {
    paddingVertical: 10,
    paddingHorizontal: 15
  },
  buttons: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    alignItems: "flex-end"
  }
})

// Note: Whether the Menu is active or not depends on Role / Step
const Menu = ({
  action: { title, skippable, waiting },
  players,
  isActive,
  hasVoted,
  onSkip,
  onVote
}) => (
    <View style={styles.menu}>
      <FlatList
        data={players}
        keyExtractor={player => player.id}
        renderItem={({ item }) => (
          <View style={styles.item}>
            <MenuItem
              {...item}
              action={(skippable || waiting) ? null : title}
              isActive={isActive}
              hasVoted={hasVoted}
              onAction={() => onVote(item.id)}
            />
          </View>
        )}
        horizontal
        showsHorizontalScrollIndicator={false}
      />
      <View style={styles.buttons}>
        {(skippable && isActive) && <Button disabled={hasVoted} onPress={onSkip}>{title}</Button>}
        {waiting && <Text>Waiting...</Text>}
      </View>
    </View>
  );

export default Menu;