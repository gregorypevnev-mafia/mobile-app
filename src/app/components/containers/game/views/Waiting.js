import React from "react";
import { Text } from "react-native";
import Placeholder from "../../../common/Placeholder";

const Waiting = () => (
  <Placeholder>
    <Text>Waiting for all players to connect...</Text>
  </Placeholder>
);

export default Waiting;
