import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Button from "../../../common/Button";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 20,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10,
  },
  result: {
    fontSize: 16,
    marginBottom: 20,
  }
});

const Finished = ({ result, onExit }) => (
  <View style={styles.container}>
    <Text style={styles.title}>Game Over</Text>
    <Text style={styles.result}>{result}</Text>
    <Button onPress={onExit}>Exit</Button>
  </View>
);

export default Finished;
