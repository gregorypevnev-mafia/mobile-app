import React from "react";
import { Text, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  time: {
    fontSize: 18,
    fontWeight: "bold"
  }
});

const Timer = ({ time }) => (
  <Text style={styles.time}>{time}</Text>
);

export default Timer;
