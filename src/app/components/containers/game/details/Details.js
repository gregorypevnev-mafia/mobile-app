import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Timer from "./Timer";
import { phaseName } from "../../../../../utilities/game";

const styles = StyleSheet.create({
  details: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "stretch",
    paddingVertical: 5,
    paddingHorizontal: 10,
    backgroundColor: "#fafafa",
    borderBottomColor: "#cdcdcd",
    borderBottomWidth: 1,
  },
  info: {
    flexGrow: 0,
    flexShrink: 0,
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "flex-start"
  },
  timer: {
    flexGrow: 0,
    flexShrink: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  phaseLabel: {
    fontSize: 16,
  },
  stepLabel: {
    fontSize: 14
  },
});

const Details = ({ step, phase, time, finished }) => (
  <View style={styles.details}>
    <View style={styles.info}>
      <Text style={styles.phaseLabel}>{finished ? "Finished" : phaseName(phase)}</Text>

      <Text style={styles.stepLabel}>{`Day ${step}`}</Text>
    </View>

    {!finished && (
      <View style={styles.timer}>
        <Timer time={time} />
      </View>
    )}
  </View>
);

export default Details;