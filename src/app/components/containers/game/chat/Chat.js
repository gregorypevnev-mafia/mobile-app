import React from "react";
import { View, FlatList, StyleSheet } from "react-native";
import Message from "./Message";
import Form from "./Form";

const styles = StyleSheet.create({
  chat: {
    flex: 1,
    flexDirection: "column",
    alignItems: "stretch"
  },
  messages: {
    flex: 1,
    backgroundColor: "#ffffff",
    borderBottomColor: "#cdcdcd",
    borderBottomWidth: 1
  },
  form: {
    flexBasis: "auto"
  }
});

const Chat = ({ messages, onMessage, isActive }) => (
  <View style={styles.chat}>
    <FlatList
      style={styles.messages}
      data={messages}
      keyExtractor={message => String(message.id)}
      renderItem={({ item }) => <Message {...item} />}
    />
    <View style={styles.form}>
      <Form onMessage={onMessage} isActive={isActive} />
    </View>
  </View>
);

export default Chat;
