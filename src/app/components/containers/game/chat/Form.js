import React, { useState } from "react";
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  form: {
    flex: 1,
    flexBasis: 40,
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  input: {
    flex: 1,
    flexGrow: 1,
    flexShrink: 1,
    padding: 10,
    backgroundColor: "#ffffff",
  },
  button: {
    flexBasis: 100,
    backgroundColor: "#0077ff",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderTopColor: "#ccc",
    borderTopWidth: 1
  },
  buttonText: {
    color: "#ffffff",
    textAlign: "center"
  }
});

const activeStyle = styles.button;

const inactiveStyle = StyleSheet.compose(styles.button, {
  opacity: 0.4,
});

const Form = ({ isActive, onMessage }) => {
  const [message, setMessage] = useState("");

  return (
    <View style={styles.form}>
      <TextInput
        editable={isActive}
        selectTextOnFocus={isActive}
        style={styles.input}
        value={message}
        placeholder="Message"
        onChangeText={setMessage}
      />

      <TouchableOpacity
        style={isActive ? activeStyle : inactiveStyle}
        disabled={!isActive}
        onPress={() => {
          if (!message || message.length === 0) return;

          onMessage(message);
          setMessage("");
        }}
      >
        <Text style={styles.buttonText}>Send</Text>
      </TouchableOpacity>
    </View>
  );
}

export default Form;
