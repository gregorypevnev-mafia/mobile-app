import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { toTime } from "../../../../../utilities/time";

const styles = StyleSheet.create({
  message: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    backgroundColor: "#ffffff",
    borderBottomColor: "#ededed",
    borderBottomWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 10
  },
  contents: {
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "stretch"
  },
  author: {
    fontSize: 14,
    fontWeight: "bold",
    marginBottom: 5
  },
  text: {
    fontSize: 12,
  },
  info: {
    fontSize: 12,
    color: "#898989",
    fontStyle: "italic"
  },
  time: {
    fontSize: 12,
  }
});

const toLines = text => {
  if (Array.isArray(text)) return text;

  return [text];
};

const Message = ({ text, by, info, at }) => (
  <View style={styles.message}>
    <View style={styles.contents}>
      {info ? (
        toLines(text).map((line, i) => (
          <Text key={i} style={styles.info}>
            {line}
          </Text>
        ))
      ) : (
          <View>
            <Text style={styles.author}>{by || "System"}</Text>
            <Text style={styles.text}>{text}</Text>
          </View>
        )}
    </View>

    <Text style={styles.time}>{toTime(at)}</Text>
  </View>
);

export default Message;
