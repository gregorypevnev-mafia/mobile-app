import React from "react";
import Waiting from "./views/Waiting";
import Finished from "./views/Finished";
import Menu from "./menu/Menu";

const GameContent = ({
  isActive,
  isWaiting,
  isFinished,
  result,
  onExit,
  ...data
}) => {
  if (isWaiting)
    return <Waiting />;

  if (isFinished)
    return <Finished result={result} onExit={onExit} />;

  return <Menu isActive={isActive} {...data} />
};

export default GameContent;