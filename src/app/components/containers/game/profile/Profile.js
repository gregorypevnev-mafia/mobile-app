import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Image from "../../../common/Image";
import { stateName, roleName } from "../../../../../utilities/game";

const styles = StyleSheet.create({
  profile: {
    flex: 1,
    flexDirection: "row",
    alignItems: "stretch",
    justifyContent: "flex-start",
    backgroundColor: "#ffffff",
    borderBottomColor: "#cdcdcd",
    borderBottomWidth: 1
  },
  image: {
    flexShrink: 0,
    flexGrow: 0,
    paddingVertical: 10,
    paddingHorizontal: 10
  },
  content: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 10,
    alignItems: "center",
    justifyContent: "space-between"
  },
  details: {
    flexShrink: 0,
    flexGrow: 0,
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "space-between"
  },
  state: {
    flexShrink: 0,
    flexGrow: 0,
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 20
  },
  usernameLabel: {
    fontSize: 18,
    fontWeight: "bold"
  },
  roleLabel: {
    fontSize: 16
  },
  stateLabel: {
    fontSize: 14,
    fontStyle: "italic"
  }
});

const Profile = ({ profile }) => (
  <View style={styles.profile}>
    <View style={styles.image}>
      <Image uri={profile.image} width="50" height="50" />
    </View>
    <View style={styles.content}>
      <View style={styles.details}>
        <Text style={styles.usernameLabel}>{profile.username}</Text>
        <Text style={styles.roleLabel}>{roleName(profile.role)}</Text>
      </View>
      <View style={styles.state}>
        <Text style={styles.stateLabel}>{stateName(profile.status)}</Text>
      </View>
    </View>
  </View>
);

export default Profile;