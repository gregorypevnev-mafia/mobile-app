import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Loading from "../common/Loading";
import Error from "../common/Error";
import Placeholder from "../common/Placeholder";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  list: {
    flexGrow: 1,
    flexShrink: 1,
  },
  status: {
    flexGrow: 0,
    flexShrink: 0
  },
})

const DEFAULT_LOADING_MESSAGE = "Loading...";
const DEFAULT_ERROR_MESSAGE = "Could not load items";
const DEFAULT_EMPTY_MESSAGE = "No items to display";

// Only mepty when there IS list in the first place (Not just nothingness)
const isEmpty = list => !list || list.length === 0;

const withPlaceholder = ({
  list,
  loading = "loading",
  error = "error",
  loadingMessage = DEFAULT_LOADING_MESSAGE,
  errorMessage = DEFAULT_ERROR_MESSAGE,
  emptyMessage = DEFAULT_EMPTY_MESSAGE,
} = {}) => {
  const ListStatus = props => {
    const isLoading = props[loading];
    const isError = props[error];
    const isLoaded = !props[loading] && !props[error];

    if (isLoading) return <Loading>{loadingMessage || DEFAULT_LOADING_MESSAGE}</Loading>;
    if (isError) return <Error>{errorMessage || DEFAULT_ERROR_MESSAGE}</Error>;
    if (isLoaded && isEmpty(props[list])) return <Text>{emptyMessage || DEFAULT_EMPTY_MESSAGE}</Text>;
    return null;
  };

  return Component => props => {
    if (isEmpty(props[list]))
      return (
        <Placeholder>
          <ListStatus {...props} />
        </Placeholder>
      );

    return (
      <View style={styles.container}>
        <View style={styles.list}>
          <Component {...props} />
        </View>
        <View style={styles.status}>
          <ListStatus {...props} />
        </View>
      </View>
    );
  }
}

export default withPlaceholder;
