import React from "react";
import { TextInput, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  input: {
    paddingVertical: 10,
    paddingHorizontal: 25,
    marginBottom: 10,
    backgroundColor: "#fefefe",
    borderColor: "#cdcdcd",
    borderRadius: 2.5,
    borderWidth: 1
  }
});

const Input = ({ onInput, value, placeholder, secure }) => (
  <TextInput
    style={styles.input}
    onChangeText={text => onInput(String(text))}
    value={value}
    placeholder={placeholder}
    secureTextEntry={secure || false}
  />
);

export default Input;