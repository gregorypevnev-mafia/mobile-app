import React from "react";
import { Text, StyleSheet, TouchableOpacity } from "react-native";

const styles = StyleSheet.create({
  buttonWrapper: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: "#0077ff",
    borderRadius: 5
  },
  buttonText: {
    color: "#fafafa",
    textAlign: "center",
    textTransform: "uppercase"
  }
});

styles.buttonWrapperDisabled = StyleSheet.compose(styles.buttonWrapper, {
  opacity: 0.6,
});

const Button = ({ children, onPress, disabled, danger }) => {
  const style = StyleSheet.compose(
    disabled ? styles.buttonWrapperDisabled : styles.buttonWrapper,
    danger ? { backgroundColor: "#ff6655" } : {}
  );

  return (
    <TouchableOpacity
      disabled={disabled || false}
      style={style}
      onPress={() => onPress && onPress()}
    >
      <Text style={styles.buttonText}>{children}</Text>
    </TouchableOpacity>
  );
}

export default Button;