import React from "react";
import { View, StyleSheet } from "react-native";
import Loading from "./Loading";
import Error from "./Error";

const styles = StyleSheet.create({
  information: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  }
});

const Information = ({ loading, error, loadingText }) => (
  <View style={styles.information}>
    {loading && <Loading>{loadingText}</Loading>}

    {(!loading && error) && <Error>{error}</Error>}
  </View>
);

export default Information;
