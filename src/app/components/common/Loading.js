import React from "react";
import { View, Text, ProgressBarAndroid, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  loadingWrapper: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 10
  },
  loadingIndicator: {
    marginRight: 10
  },
  loadingText: {
    fontStyle: "italic",
    fontSize: 12,
  },
});

const Loading = ({ children }) => (
  <View style={styles.loadingWrapper}>
    <View style={styles.loadingIndicator}>
      <ProgressBarAndroid />
    </View>
    <Text style={styles.loadingText}>
      {children || "Loading..."}
    </Text>
  </View>
);

export default Loading;