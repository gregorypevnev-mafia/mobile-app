import React from "react";
import { Text, StyleSheet, TouchableOpacity } from "react-native";

const styles = StyleSheet.create({
  linkWrapper: {
    borderBottomColor: "#0077ff",
    borderBottomWidth: 1,
    paddingBottom: 2
  },
  linkText: {
    color: "#0077ff"
  }
});

const Link = ({ children, onPress }) => (
  <TouchableOpacity style={styles.linkWrapper} onPress={() => onPress && onPress()}>
    <Text style={styles.linkText}>{children}</Text>
  </TouchableOpacity>
);

export default Link;