import React from "react";
import { Text, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  heading: {
    fontSize: 20,
    fontWeight: "bold",
    borderBottomColor: "#333",
    borderBottomWidth: 1,
    paddingBottom: 3,
    marginBottom: 15
  }
});

const Heading = ({ children }) => (
  <Text style={styles.heading}>{children}</Text>
);

export default Heading;