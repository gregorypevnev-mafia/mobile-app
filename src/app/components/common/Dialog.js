import React, { useEffect } from "react";
import { View, TouchableOpacity, StyleSheet, BackHandler } from "react-native";
import { Ionicons } from "@expo/vector-icons";

const styles = StyleSheet.create({
  overlay: {
    flex: 1,
    flexGrow: 1,
    flexShrink: 1,
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    paddingHorizontal: 30,
    paddingVertical: 50,
    minHeight: 400,
    backgroundColor: "rgba(0, 0, 0, 0.3)",
    zIndex: 100,
    justifyContent: "center",
    alignItems: "stretch",
  },
  dialog: {
    justifyContent: "center",
    alignItems: "stretch",
    backgroundColor: "#fafafa",
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  close: {
    position: "absolute",
    top: 0,
    right: 0,
    width: 30,
    height: 30,
    padding: 0,
    justifyContent: "center",
    alignItems: "stretch"
  }
});

const Dialog = ({ children, isOpen, onClose }) => {
  const backListener = active => () => {
    console.log("BACK", active);

    if (active) onClose();

    return active; // Overriding if the dialog is open
  }

  useEffect(() => {
    const backHandler = BackHandler.addEventListener("hardwareBackPress", backListener(isOpen));

    return () => backHandler.remove();
  }, [isOpen]);

  return isOpen && (
    <View style={styles.overlay}>
      <View style={styles.dialog}>
        {children}
      </View>
      {onClose && (
        <TouchableOpacity style={styles.close} onPress={onClose}>
          <Ionicons name="ios-close" size={50} color="white" />
        </TouchableOpacity>
      )}
    </View>
  )
};

export default Dialog;