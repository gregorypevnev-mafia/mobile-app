import React from "react";
import { Text, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  error: {
    fontSize: 14,
    fontStyle: "italic",
    color: "#ff4400",
    textAlign: "right"
  }
});

const Error = ({ children }) => (
  <Text style={styles.error}>{children}</Text>
);

export default Error;