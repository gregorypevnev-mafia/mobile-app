import React from "react";
import { TouchableOpacity, StyleSheet } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import resources from "../../resources";

const styles = StyleSheet.create({
  fab: {
    borderRadius: 25,
    borderWidth: 0,
    backgroundColor: "#0077ff",
    padding: 20,
    width: 50,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    ...resources.shadow,
  }
});

const Fab = ({ iconName, onPress }) => (
  <TouchableOpacity style={styles.fab} onPress={() => onPress && onPress()}>
    <Ionicons name={iconName || "ios-add"} size={25} color="white" />
  </TouchableOpacity>
);

export default Fab;
