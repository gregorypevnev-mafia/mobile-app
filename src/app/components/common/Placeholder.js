import React from "react";
import { View, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  content: {
    flex: 0
  }
});

const Placeholder = ({ children }) => (
  <View style={styles.wrapper}>
    <View style={styles.content}>{children}</View>
  </View>
);

export default Placeholder;
