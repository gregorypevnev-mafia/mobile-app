import React from "react";
import { Image, StyleSheet } from "react-native";
import Constants from "expo-constants";

const CONFIG = Constants.manifest.extra;

const BLANK_IMAGE = require("../../../../assets/blank.png");

const URI_PATTERN = /^https?\:\/\/[a-z0-9]+(\:[0-9]+)?\/.+\.(png|jpe?g|tiff)$/;

const DEFAULT_WIDTH = 100;
const DEFAULT_HEIGHT = 100;

const isURI = uri => uri && URI_PATTERN.test(uri);

const fullURI = uri => `${CONFIG.API_URL}/images/${uri}`;

const imageSource = uri => isURI(uri) ? uri : ({ uri: fullURI(uri) });

const styles = StyleSheet.create({
  image: {
    marginBottom: 16
  },
});

const ImageWithDefault = ({ uri, width, height }) => (
  <Image
    style={{
      ...styles.image,
      width: Number(width) || DEFAULT_WIDTH,
      height: Number(height) || DEFAULT_HEIGHT,
    }}
    defaultSource={BLANK_IMAGE}
    source={imageSource(uri)}
  // Note: Could also add some kind of caching - Device Feature???
  />
);

export default ImageWithDefault;