import React from "react";
import { View, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: "#fafafa",
  }
});

const Container = ({ children }) => (
  <View style={styles.content}>
    {children}
  </View>
);

export default Container