import React from "react";
import { View, ScrollView, FlatList, StyleSheet } from "react-native";
import Fab from "../common/Fab";
import Dialog from "../common/Dialog";

const styles = StyleSheet.create({
  page: {
    flex: 1,
    flexGrow: 1,
    flexShrink: 1,
    flexDirection: "column",
    position: "relative",
    minHeight: 400
  },
  contents: {
    flex: 1,
    flexDirection: "column",
    paddingVertical: 20,
    paddingHorizontal: 10
  },
  control: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
    flexBasis: 60,
    flexGrow: 0,
    flexShrink: 1,
    paddingVertical: 10,
    paddingHorizontal: 15,
    backgroundColor: "#fefefe",
    borderTopColor: "#cdcdcd",
    borderTopWidth: 1
  },
  fab: {
    position: "absolute",
    right: 20,
    bottom: 15,
  },
});

const Page = ({ children, items, info, dialog, showDialog, onOpenDialog, onCloseDialog }) => (
  <View style={styles.page}>
    {!items ? (
      <ScrollView style={styles.contents}>
        {children}
      </ScrollView>
    ) : (
        // Pretty awkward, but FlatList cannot be rendered inside of ScrollView 
        //   - Otherwise all of its optimizations are not taken advanntage of
        <View style={styles.contents}>
          <FlatList
            data={items}
            keyExtractor={item => item.name}
            renderItem={({ item }) => item.content}
          />
        </View>
      )}

    {info && (
      <View style={styles.control}>
        {info}
      </View>
    )}

    {dialog && (
      <>
        <View style={styles.fab}>
          <Fab onPress={() => onOpenDialog && onOpenDialog()} />
        </View>

        <Dialog
          isOpen={showDialog}
          onClose={() => onCloseDialog && onCloseDialog()}
        >
          {dialog}
        </Dialog>
      </>
    )}
  </View>
);

export default Page;
