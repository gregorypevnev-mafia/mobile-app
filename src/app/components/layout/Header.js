import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Button from "../common/Button";
import Link from "../common/Link";

const styles = StyleSheet.create({
  header: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#f8f8f8",
    borderBottomColor: "#cdcdcd",
    borderBottomWidth: 1
  },
  heading: {
    flex: 10,
    fontSize: 25,
    fontWeight: "bold",
    paddingVertical: 5
  },
  actions: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  actionsAuthed: {
    flex: 11
  },
  actionsUnauthed: {
    flex: 5
  },
  label: {
    fontStyle: "italic",
  }
});

const actionsAuthedStyle = StyleSheet.compose(styles.actions, styles.actionsAuthed);
const actionsUnauthedStyle = StyleSheet.compose(styles.actions, styles.actionsUnauthed);

const formatUsername = username => {
  if (!username) return "???";
  if (username.length < 10) return username;
  return `${username.slice(0, 10)}...`
}

const Header = ({ user, signIn, signUp, signOut } = {}) => (
  <View style={styles.header}>
    <Text style={styles.heading}>Mafia</Text>

    {user ? (
      <View style={actionsAuthedStyle}>
        <Text style={styles.label}>{formatUsername(user)}</Text>
        <Button onPress={() => signOut && signOut()}>Sign Out</Button>
      </View>
    ) : (
        <View style={actionsUnauthedStyle}>
          <Link onPress={() => signIn && signIn()}>Sign In</Link>
          <Link onPress={() => signUp && signUp()}>Sign Up</Link>
        </View>
      )}
  </View>
);

export default Header;