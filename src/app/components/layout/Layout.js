import React from "react";
import { View, StyleSheet } from "react-native";
import Header from "./Header";
import Container from "./Container";

const styles = StyleSheet.create({
  main: {
    flex: 1
  },
  header: {
    flexGrow: 0,
    flexShrink: 0
  },
  content: {
    flexGrow: 1,
    flexShrink: 1
  }
});

const Layout = ({ children, user, signIn, signUp, signOut }) => (
  <View style={styles.main}>
    <View style={styles.header}>
      <Header {...{ user, signIn, signUp, signOut }} />
    </View>
    <View style={styles.content}>
      <Container>{children}</Container>
    </View>
  </View>
);

export default Layout;
