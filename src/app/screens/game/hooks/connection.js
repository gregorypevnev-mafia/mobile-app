import { useEffect } from "react";

const useConnection = ({ loaded, joined, error }, onLoad, onConnect) => {
  const shouldJoin = loaded && !error && !joined;

  useEffect(() => {
    onLoad();
  }, []);

  useEffect(() => {
    if (shouldJoin) onConnect();
  }, [loaded]);
};

export default useConnection;
