import React from "react";
import { connect } from "react-redux";
import useConnection from "./hooks/connection";
import useBack from "../../hooks/back";
import useTimer from "../../hooks/timer";
import useMessages from "../../hooks/messages";
import LoadingView from "./components/LoadingView";
import ErrorView from "./components/ErrorView";
import Game from "../../components/containers/game/Game";
import { phaseDuration } from "../../../utilities/game";
import { mapStateToProps, mapDispatchToProps } from "./container";

const GameScreen = ({
  // Status
  isActive,
  isWaiting,
  isFinished,

  // Data
  loaded,
  joined,
  error,
  stage,
  player,
  players,
  messages,
  action,
  result,

  // Getters
  loading,
  leaving,
  hasVoted,
  resumed,

  // Actions
  loadGame,
  joinGame,
  leaveGame,
  vote,
  message,
  clear,

  // Navigation
  navigation,

  route: { params: { gameId } },
}) => {
  const leave = () => {
    leaveGame();

    navigation.pop();
  }

  useConnection(
    { loaded, joined, error },
    () => loadGame(gameId),
    () => joinGame(player.username, gameId)
  );

  useBack("You will be leaving the game", () => leaving, leave);

  const currentPhase = stage ? stage.phase : null;

  const time = useTimer(currentPhase ? phaseDuration(currentPhase) : 0, currentPhase, resumed);

  useMessages(messages, null, clear);

  if (error)
    return (
      <ErrorView error={error} />
    );

  if (loading)
    return (
      <LoadingView
        loaded={loaded}
        joined={joined}
      />
    );

  return (
    <Game
      isActive={(isActive && time > 0)}
      isWaiting={isWaiting}
      isFinished={isFinished}
      hasVoted={hasVoted}
      stage={stage}
      player={player}
      players={players}
      messages={messages}
      time={time}
      action={action}
      result={result}
      onVote={playerId => vote(playerId)}
      onSkip={() => vote(null)}
      onMessage={text => message(player, text)}
      onExit={leave}
    />
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(GameScreen);
