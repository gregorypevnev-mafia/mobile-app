import {
  RESUMED_STATUS,
  PAUSED_STATUS,
  FINISHED_STATUS,
  isInfoMessage,
  votablePlayers,
  actionInfo,
  resultMessage,
  isActive,
} from "../../../../utilities/game";

const playersToList = players => Object.keys(players).map(id => players[id]);

const groupPlayers = (players, user) => {
  const playersList = playersToList(players || {});

  return playersList.reduce(({ current, other }, player) => {
    if (player.id === user.id)
      return {
        current: player,
        other,
      }

    return {
      current,
      other: [...other, player],
    }
  }, { current: null, other: [] });
};

const processMessages = messages =>
  messages.map((content, i) => ({
    id: i,
    info: isInfoMessage(content),
    ...content,
  }));

const isWaiting = ({ status }) => status === PAUSED_STATUS;

const isFinished = ({ status }) => status === FINISHED_STATUS;

const mapData = ({
  user: {
    user
  },
  play,
  messages: {
    play: playMessages,
  },
}) => {
  const {
    loaded,
    joined,
    error,
    stage,
    players,
    status,
    voted,
    result,
  } = play;

  const { current, other } = groupPlayers(players, user);

  const messages = processMessages(playMessages);

  const loading = !loaded || !joined;
  const joining = loaded && !joined;
  const leaving = finished || error;

  const active = isActive(play, current);
  const waiting = isWaiting(play);
  const finished = isFinished(play);

  return {
    isActive: active,
    isWaiting: waiting,
    isFinished: finished,
    hasVoted: voted,
    resumed: status === RESUMED_STATUS,

    loaded,
    joined,
    error,
    status,

    stage,
    player: current || user, // Falling back to regular data without enrichment
    players: stage ? votablePlayers(stage, other) : [],

    action: stage ? actionInfo(stage) : null,

    messages,

    loading,
    joining,
    leaving,

    result: resultMessage(result),
  }
};

export default mapData;
