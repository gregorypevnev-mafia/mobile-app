import { bindActionCreators } from "redux";
import { loadGame, joinGame, leaveGame, vote } from "../../../../store/play/actions";
import { send, clearMessages } from "../../../../store/messages/actions";

const mapActions = dispatch => bindActionCreators({
  loadGame,
  joinGame,
  leaveGame,
  vote,
  message: send,
  clear: clearMessages,
}, dispatch);

export default mapActions;