import React from "react";
import { View, StyleSheet } from "react-native";
import Loading from "../../../components/common/Loading";

const styles = StyleSheet.create({
  view: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

const LoadingView = ({ loaded, joined }) => (
  <View style={styles.view}>
    <Loading>
      {!loaded && "Loading game data..."}
      {(loaded && !joined) && "Joining the game..."}
    </Loading>
  </View>
);

export default LoadingView;
