import React from "react";
import { View, Text, StyleSheet } from "react-native";

const DEFAULT_ERROR = "Could not join the game";

const styles = StyleSheet.create({
  view: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  message: {
    color: "#ff5533",
    fontStyle: "italic"
  }
});

const ErrorView = ({ error }) => (
  <View style={styles.view}>
    <Text style={styles.message}>{error || DEFAULT_ERROR}</Text>
  </View>
);

export default ErrorView;
