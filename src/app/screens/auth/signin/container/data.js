const mapData = ({ user: { signInProgress, signInError } }) => ({
  progress: signInProgress,
  error: signInError,
});

export default mapData;
