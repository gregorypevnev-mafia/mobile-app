import { bindActionCreators } from "redux";
import { signIn, signUp } from "../../../../../store/user/actions";

const mapActions = dispatch => bindActionCreators({
  signIn,
  signUp,
}, dispatch);

export default mapActions;
