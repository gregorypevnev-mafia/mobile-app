import React from "react";
import { connect } from "react-redux";
import Layout from "../../../components/layout/Layout";
import Page from "../../../components/layout/Page";
import Heading from "../../../components/common/Heading";
import Button from "../../../components/common/Button";
import Loading from "../../../components/common/Loading";
import SignInForm from "./components/SignInForm";
import { mapDispatchToProps, mapStateToProps } from "./container";

const SignInScreen = ({ progress, error, signIn, navigation }) => (
  <Layout signUp={() => navigation.replace("Sign Up")}>
    <Page info={(
      progress && (
        <Loading>Signing in...</Loading>
      )
    )}>
      <Heading>Sign In</Heading>
      <SignInForm isDisabled={progress} onSubmit={signIn} error={error} />
      <Button onPress={() => navigation.replace("Sign Up")}>No Account?</Button>
    </Page>
  </Layout>
);

export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen);