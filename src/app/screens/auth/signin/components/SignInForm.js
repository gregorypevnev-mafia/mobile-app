import React from "react";
import { View, StyleSheet } from "react-native";
import Input from "../../../../components/common/Input";
import Button from "../../../../components/common/Button";
import Error from "../../../../components/common/Error";
import { useSignIn } from "../hooks/useSignIn";

const styles = StyleSheet.create({
  form: {
    marginBottom: 15,
  },
  error: {
    marginBottom: 10,
  }
})

const SignInForm = ({
  onSubmit,
  isDisabled,
  error
}) => {
  const [
    { data, isValid, isSubmitted },
    { setUsername, setPassword, submit },
  ] = useSignIn(error);

  const active = isValid && !isDisabled;

  return (
    <View style={styles.form}>
      <Input
        value={data.username}
        placeholder="Username"
        onInput={setUsername}
      />
      <Input
        value={data.password}
        placeholder="Password"
        onInput={setPassword}
        secure
      />

      {(isSubmitted && error) && (
        <View style={styles.error}>
          <Error>{error}</Error>
        </View>
      )}

      <Button
        disabled={!active}
        onPress={() => {
          submit();
          onSubmit(data);
        }}
      >
        Sign In
      </Button>
    </View>
  )
};

export default SignInForm;
