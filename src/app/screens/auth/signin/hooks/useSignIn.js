import { useState, useEffect } from "react";
import { validateSignInData } from "../../utils/validation";

export const useSignIn = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [submitted, setSubmitted] = useState(false);

  useEffect(() => {
    setSubmitted(false);
  }, [username, password]);

  const signInData = {
    username,
    password,
  };

  const state = {
    data: signInData,
    isValid: validateSignInData(signInData),
    isSubmitted: submitted,
  };

  const actions = {
    setUsername,
    setPassword,
    submit: setSubmitted.bind(null, true),
  };

  return [state, actions];
};
