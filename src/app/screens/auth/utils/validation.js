const REQUIRED_USERNAME_LENGTH = 6;
const REQUIRED_PASSWORD_LENGTH = 6;

const isValidUsername = (username, strict = false) =>
  (!!username && (username.length >= (strict ? REQUIRED_USERNAME_LENGTH : 0)));

const isValidPassword = (password, strict = false) =>
  (!!password && (password.length >= (strict ? REQUIRED_PASSWORD_LENGTH : 0)));

const isValidConfirm = (confirm, check) =>
  (!!confirm && confirm === check);

export const validateSignInData = ({ username, password }) =>
  isValidUsername(username) && isValidPassword(password);

export const validateSignUpData = ({ username, password, confirm }) =>
  isValidUsername(username) && isValidPassword(password) && isValidConfirm(confirm, password);
