import React from "react";
import { View, StyleSheet } from "react-native";
import Input from "../../../../components/common/Input";
import Button from "../../../../components/common/Button";
import Error from "../../../../components/common/Error";
import ImagePicker from "./ImagePicker";
import { useSignUp } from "../hooks/useSignUp";

const styles = StyleSheet.create({
  form: {
    marginBottom: 15,
  },
  error: {
    marginBottom: 10,
  }
})

const SignUpForm = ({
  onSubmit,
  isDisabled,
  error
}) => {
  const [
    { data, isValid, isSubmitted },
    { setUsername, setPassword, setConfirm, setImage, submit },
  ] = useSignUp(error);

  const active = isValid && !isDisabled;

  return (
    <View style={styles.form}>
      <ImagePicker
        image={data.image}
        onImage={setImage}
      />
      <Input
        value={data.username}
        placeholder="Username"
        onInput={setUsername}
      />
      <Input
        value={data.password}
        placeholder="Password"
        onInput={setPassword}
        secure
      />
      <Input
        value={data.confirm}
        placeholder="Confirm Password"
        onInput={setConfirm}
        secure
      />

      {(isSubmitted && error) && (
        <View style={styles.error}>
          <Error>{error}</Error>
        </View>
      )}

      <Button
        disabled={!active}
        onPress={() => {
          submit();

          onSubmit(data);
        }}
      >
        Sign Up
      </Button>
    </View>
  )
};

export default SignUpForm;
