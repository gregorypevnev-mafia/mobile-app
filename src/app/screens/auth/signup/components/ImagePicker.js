import React from "react";
import {
  Image,
  View,
  TouchableOpacity,
  StyleSheet,
  Alert,
  Platform,
} from "react-native";
import { pickCamera, pickLibrary } from "../utils/picker";

const DEFAULT_IMAGE = require("../../../../../../assets/default.png");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 20,
  },
  image: {
    width: 150,
    height: 150
  },
});

const picker = ({ onImage }) => async () => {
  const onPick = result => result && onImage(result);

  Alert.alert(
    "Profile Image",
    "Select which source you would like to use",
    [
      {
        text: "Camera",
        onPress: () => pickCamera().then(onPick)
      },
      {
        text: "Library",
        onPress: () => pickLibrary().then(onPick)
      }
    ],
    { cancelable: true }
  );
};

const profileImage = image => {
  if (image) return { uri: image };

  return DEFAULT_IMAGE;
}

const ProfileImagePicker = ({ image, onImage }) => {
  if (Platform.OS === "web") return null;

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={picker({ onImage })}>
        <Image style={styles.image} source={profileImage(image)} />
      </TouchableOpacity>
    </View>
  );
}

export default ProfileImagePicker;
