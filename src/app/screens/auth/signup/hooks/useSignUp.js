import { useState, useEffect } from "react";
import { validateSignUpData } from "../../utils/validation";

export const useSignUp = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [confirm, setConfirm] = useState("");
  const [image, setImage] = useState(null);
  const [submitted, setSubmitted] = useState(false);

  useEffect(() => {
    setSubmitted(false);
  }, [username, password, confirm]);

  const signUpData = {
    username,
    password,
    confirm,
    image,
  };

  const state = {
    data: signUpData,
    isValid: validateSignUpData(signUpData),
    isSubmitted: submitted,
  };

  const actions = {
    setUsername,
    setPassword,
    setConfirm,
    setImage,
    submit: setSubmitted.bind(null, true),
  };

  return [state, actions];
};
