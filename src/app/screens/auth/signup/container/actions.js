import { bindActionCreators } from "redux";
import { signUp } from "../../../../../store/user/actions";

const mapActions = dispatch => bindActionCreators({
  signUp,
}, dispatch);

export default mapActions;
