const mapData = ({ user: { signUpProgress, signUpError } }) => ({
  progress: signUpProgress,
  error: signUpError,
});

export default mapData;
