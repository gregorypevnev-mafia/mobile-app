import * as ImagePicker from "expo-image-picker";

const IMAGE_SETTINGS = {
  mediaTypes: ImagePicker.MediaTypeOptions.Images,
  allowsEditing: false,
  aspect: [3, 3],
  quality: 1,
};

const processPick = ({ cancelled, uri }) => cancelled ? null : uri;

export const pickCamera = () => ImagePicker.launchCameraAsync(IMAGE_SETTINGS).then(processPick);

export const pickLibrary = () => ImagePicker.launchImageLibraryAsync(IMAGE_SETTINGS).then(processPick);
