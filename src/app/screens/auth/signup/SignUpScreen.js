import React from "react";
import { connect } from "react-redux";
import Layout from "../../../components/layout/Layout";
import Page from "../../../components/layout/Page";
import Heading from "../../../components/common/Heading";
import Button from "../../../components/common/Button";
import Loading from "../../../components/common/Loading";
import SignUpForm from "./components/SignUpForm";
import { mapStateToProps, mapDispatchToProps } from "./container";

const SignUpScreen = ({ progress, error, signUp, navigation }) => (
  <Layout signIn={() => navigation.replace("Sign In")}>
    <Page info={(
      progress && (
        <Loading>Signing up...</Loading>
      )
    )}>
      <Heading>Sign Up</Heading>
      <SignUpForm isDisabled={progress} onSubmit={signUp} error={error} />
      <Button onPress={() => navigation.replace("Sign In")}>I Already Have An Account</Button>
    </Page>
  </Layout>
);

export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);
