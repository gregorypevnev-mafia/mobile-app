import mapActions from "./actions";
import mapData from "./data";

export const mapDispatchToProps = mapActions;
export const mapStateToProps = mapData;