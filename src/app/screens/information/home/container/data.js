import { shouldLoad } from "../../utils/shouldLoad";

const mapData = ({
  user: {
    loading,
    error,
    user
  }
}) => ({
  loading,
  error,
  user,
  shouldLoad: shouldLoad(loading, error),
});

export default mapData;