import { bindActionCreators } from "redux";
import { loadProfile } from "../../../../../store/user/actions";

const mapActions = dispatch => bindActionCreators({ loadProfile }, dispatch);

export default mapActions;