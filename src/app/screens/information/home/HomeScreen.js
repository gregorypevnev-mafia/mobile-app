import React, { useEffect } from "react";
import { connect } from "react-redux";
import Page from "../../../components/layout/Page";
import Heading from "../../../components/common/Heading";
import Error from "../../../components/common/Error";
import Loading from "../../../components/common/Loading";
import Information from "../../../components/common/Information";
import Profile from "./components/Profile";
import { mapStateToProps, mapDispatchToProps } from "./container";

const HomeScreen = ({
  loading,
  error,
  user,
  shouldLoad,
  loadProfile,
}) => {
  const isReady = !loading && !error;

  useEffect(() => {
    if (shouldLoad) loadProfile();
  }, []);

  return (
    <Page info={(
      !isReady && (
        <Information
          loading={loading}
          error={error}
          loadingText="Loading profile information"
        />
      )
    )}>
      <Heading>Welcome, {user.username}</Heading>

      {user && <Profile user={user} />}
    </Page>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
