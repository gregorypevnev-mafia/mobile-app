import React from "react";
import { View, StyleSheet } from "react-native";
import Image from "../../../../components/common/Image";
import Joined from "./Joined";

const styles = StyleSheet.create({
  content: {
    alignItems: "center"
  },
  image: {
    marginBottom: 20,
    elevation: 3,
  },
  info: {
    alignSelf: "center"
  }
});

const Profile = ({ user: { image, at } }) => (
  <View style={styles.content}>
    <View style={styles.image}>
      <Image uri={image} width="200" height="200" />
    </View>
    <View style={styles.info}>
      <Joined at={at} />
    </View>
  </View>
);

export default Profile;