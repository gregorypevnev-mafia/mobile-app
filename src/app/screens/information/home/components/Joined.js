import React from "react";
import { Text, StyleSheet } from "react-native";
import { toDate } from "../../../../../utilities/time";

const styles = StyleSheet.create({
  text: {
    fontSize: 16,
    fontStyle: "italic"
  }
});

const Joined = ({ at }) => (
  <Text style={styles.text}>{at && `Joined: ${toDate(at)}`}</Text>
);

export default Joined;
