import { StyleSheet } from "react-native";

export const listStyles = StyleSheet.create({
  list: {
    flex: 1
  },
  item: {
    borderBottomColor: "#ddd",
    borderBottomWidth: 1
  }
});