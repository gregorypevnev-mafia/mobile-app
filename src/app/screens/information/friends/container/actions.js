import { bindActionCreators } from "redux";
import {
  loadFriends,
  deleteFriend,
  requestFriend,
  rejectRequest,
  acceptRequest
} from "../../../../../store/friends/actions";

const mapActions = dispatch => bindActionCreators({
  loadFriends,
  deleteFriend,
  requestFriend,
  rejectRequest,
  acceptRequest,
}, dispatch);

export default mapActions;
