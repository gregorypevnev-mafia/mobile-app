import { createDenormalizer } from "../../../../../utilities/normalization";
import { shouldLoad } from "../../utils/shouldLoad";

const requestsDenormalizer = createDenormalizer();
const friendsDenormalizer = createDenormalizer();

const mapData = ({
  friends: {
    loading,
    submitting,
    error,
    submissionError,
    friends,
    requests,
    submissionData
  }
}) => {
  const friendsList = friendsDenormalizer.list(friends);
  const requestsList = requestsDenormalizer.list(requests);

  return {
    loading,
    submitting,
    error,
    submissionError,
    friends: friendsList,
    requests: requestsList,
    submissionData: submissionData || {},
    shouldLoad: shouldLoad(loading, error),
  };
};

export default mapData;