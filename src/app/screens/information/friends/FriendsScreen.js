import React, { useEffect } from "react";
import { View, StyleSheet } from "react-native";
import { connect } from "react-redux";
import Page from "../../../components/layout/Page";
import Heading from "../../../components/common/Heading";
import FriendsList from "./components/friends/FriendsList";
import RequestsList from "./components/requests/RequestsList";
import RequestForm from "./components/RequestForm";
import useDialog from "../../../hooks/dialog";
import { mapStateToProps, mapDispatchToProps } from "./container";

const styles = StyleSheet.create({
  list: {
    minHeight: 250,
    marginBottom: 15,
  }
});

const FriendsScreen = ({
  loading,
  submitting,
  error,
  submissionError,
  friends,
  requests,
  submissionData,
  shouldLoad,
  loadFriends,
  deleteFriend,
  requestFriend,
  rejectRequest,
  acceptRequest,
}) => {
  const sendRequest = async data => {
    const requested = await requestFriend(data);

    if (requested) closeDialog();
  };

  const [isOpen, { openDialog, closeDialog }] = useDialog();

  useEffect(() => {
    if (shouldLoad) loadFriends();
  }, []);

  return (
    <Page
      dialog={(
        <RequestForm
          submitting={submitting}
          error={submissionError}
          defaults={submissionData}
          onSubmit={sendRequest}
        />
      )}
      items={[{
        name: "requests",
        content: (<View style={styles.list}>
          <Heading>Requests</Heading>
          <RequestsList
            loading={loading}
            error={error}
            requests={requests}
            onRejected={rejectRequest}
            onAccepted={acceptRequest}
          />
        </View>)
      }, {
        name: "friends",
        content: (<View style={styles.list}>
          <Heading>Friends</Heading>
          <FriendsList
            loading={loading}
            error={error}
            friends={friends} onDelete={deleteFriend}
          />
        </View>)
      }]}
      showDialog={isOpen}
      onCloseDialog={closeDialog}
      onOpenDialog={openDialog}
    />
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(FriendsScreen);
