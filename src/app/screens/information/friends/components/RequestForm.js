import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import Heading from "../../../../components/common/Heading";
import Input from "../../../../components/common/Input";
import Button from "../../../../components/common/Button";
import Error from "../../../../components/common/Error";

const styles = StyleSheet.create({
  form: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexBasis: 200,
    justifyContent: "space-between",
    alignItems: "stretch"
  },
  title: {
    marginBottom: 10,
  }
});

const RequestForm = ({
  submitting,
  error,
  defaults,
  onSubmit
}) => {
  const [username, setUsername] = useState(defaults.username || "");

  const isDisabled = username.length === 0 || submitting;

  return (
    <View style={styles.form}>
      <View>
        <Heading>Send a Friend Request</Heading>
        <Input onInput={setUsername} placeholder="Username" value={username} />
      </View>
      <View>
        {error && <Error>{error}</Error>}
        <Button disabled={isDisabled} onPress={() => onSubmit({ username })}>Send</Button>
      </View>
    </View>
  );
};

export default RequestForm;
