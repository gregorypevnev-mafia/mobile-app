import React from "react";
import { View, FlatList } from "react-native";
import Request from "./Request";
import { listStyles as styles } from "../../../styles";
import withPlaceholder from "../../../../../components/hoc/withPlaceholder";

const RequestsList = ({ requests, onAccepted, onRejected }) => (
  <View style={styles.list}>
    <FlatList
      data={requests}
      keyExtractor={request => request.requestId}
      renderItem={({ item }) => (
        <View style={styles.item}>
          <Request
            from={item.user}
            requested={item.at}
            accepting={item.accepting}
            rejecting={item.rejecting}
            onAccept={() => onAccepted(item.requestId)}
            onReject={() => onRejected(item.requestId)}
          />
        </View>
      )}
    />
  </View>
);

const placeholderSettings = {
  list: "requests",
  emptyMessage: "No new friend requests",
  errorMessage: "Could not load list of friend requests",
};

export default withPlaceholder(placeholderSettings)(RequestsList);
