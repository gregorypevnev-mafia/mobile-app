import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Button from "../../../../../components/common/Button";
import Image from "../../../../../components/common/Image";
import { toDate } from "../../../../../../utilities/time";

const styles = StyleSheet.create({
  request: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 15,
    backgroundColor: "#fefefe"
  },
  requestTitle: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 5,
  },
  requestContent: {
    alignItems: "center"
  },
  requestText: {
    fontSize: 16,
  },
  requestInfo: {
    fontSize: 14,
    fontStyle: "italic",
    marginTop: 5
  },
  requestActions: {
    height: 80,
    flexDirection: "column",
    justifyContent: "space-between"
  }
});

const Request = ({
  from: {
    username,
    image,
  },
  requested,
  accepting,
  rejecting,
  onAccept,
  onReject
}) => {
  const processing = accepting || rejecting;

  return (
    <View style={styles.request}>
      <View style={styles.requestContent}>
        <Text style={styles.requestTitle}>{username}</Text>
        <Image uri={image} width="80" height="80" />
        <Text style={styles.requestInfo}>{toDate(requested)}</Text>
      </View>
      <View style={styles.requestActions}>
        <Button
          disabled={processing}
          onPress={() => onAccept && onAccept()}
        >
          {accepting ? "Accepting..." : "Accept"}
        </Button>
        <Button
          disabled={processing}
          onPress={() => onReject && onReject()}
          danger
        >
          {rejecting ? "Rejecting..." : "Reject"}
        </Button>
      </View>
    </View>
  );
};

export default Request;
