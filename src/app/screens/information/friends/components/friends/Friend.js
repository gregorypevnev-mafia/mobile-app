import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Button from "../../../../../components/common/Button";
import Image from "../../../../../components/common/Image";

const styles = StyleSheet.create({
  friend: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 15,
    backgroundColor: "#fefefe",
  },
  friendTitle: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 5,
  },
  friendContent: {
    alignItems: "center"
  },
  friendActions: {}
});

const Friend = ({ friend: { username, image }, deleting, onDelete }) => (
  <View style={styles.friend}>
    <View style={styles.friendContent}>
      <Text style={styles.friendTitle}>{username}</Text>
      <Image uri={image} width="80" height="80" />
    </View>
    <View style={styles.friendActions}>
      <Button disabled={deleting} onPress={() => onDelete && onDelete()} danger>
        {deleting ? "Unfriending..." : "Unfriend"}
      </Button>
    </View>
  </View>
);

export default Friend;
