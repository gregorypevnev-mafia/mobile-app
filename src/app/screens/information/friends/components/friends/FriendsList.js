import React from "react";
import { View, FlatList } from "react-native";
import Friend from "./Friend";
import { listStyles as styles } from "../../../styles";
import withPlaceholder from "../../../../../components/hoc/withPlaceholder";

const FriendsList = ({ friends, onDelete }) => (
  <View style={styles.list}>
    <FlatList
      data={friends}
      keyExtractor={friend => friend.friendId}
      renderItem={({ item }) => (
        <View style={styles.item}>
          <Friend
            friend={item.user}
            deleting={item.deleting}
            onDelete={() => onDelete(item.friendId)}
          />
        </View>
      )}
    />
  </View>
);

const placeholderSettings = {
  list: "friends",
  emptyMessage: "No friends",
  errorMessage: "Could not load list of friends",
};

export default withPlaceholder(placeholderSettings)(FriendsList);