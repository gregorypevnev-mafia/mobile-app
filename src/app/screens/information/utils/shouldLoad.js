export const shouldLoad = (loading, error) => !loading && !!error;
