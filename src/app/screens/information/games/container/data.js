import { createDenormalizer } from "../../../../../utilities/normalization";
import { shouldLoad } from "../../utils/shouldLoad";
import { gamesWithResults } from "../../../../../utilities/game";

const withUsername = ({ by, ...data }) => ({
  ...data,
  by: by.username,
});

const formatInvitations = (invitations = []) =>
  invitations.map(({ id, accepting, rejecting, ...invitation }) => ({
    id,
    invitation: withUsername(invitation),
    accepting,
    rejecting,
  }));

const formatGames = (games, user) =>
  (games || []).map(({ id, deleting, ...game }) => ({
    id,
    game: withUsername(game),
    deleting,
    isOwned: user.id === game.by.id,
  }));

const formatFriends = friends =>
  friends.map(({ user }) => ({
    id: user.id,
    user,
  }));

const PAST_CATEGORY = "PAST_CATEGORY";
const CURRENT_CATEGORY = "CURRENT_CATEGORY";
const FUTURE_CATEGORY = "FUTURE_CATEGORY";

const gameCategorizer = game => {
  switch (game.status) {
    case "WAITING":
    case "CANCELLED":
      return FUTURE_CATEGORY;
    case "FINISHED":
      return PAST_CATEGORY;
    case "PLAYING":
    default:
      return CURRENT_CATEGORY;
  }
};

const invitationsDenormalizer = createDenormalizer();
const friendsDenormalizer = createDenormalizer();
const gamesDenormalizer = createDenormalizer({
  categorizer: gameCategorizer
});

const mapData = ({
  user: {
    user,
  },
  games: {
    loading,
    submitting,
    error,
    submissionError,
    invitations,
    games,
    submissionData,
  },
  friends,
}) => {
  const invitationsList = invitationsDenormalizer.list(invitations);
  const gamesCategories = gamesDenormalizer.categories(games);
  const friendsList = friendsDenormalizer.list(friends.friends);

  return {
    loading,
    submitting,
    error,
    submissionError,
    invitations: formatInvitations(invitationsList),
    pastGames: gamesWithResults(formatGames(gamesCategories[PAST_CATEGORY], user)),
    currentGames: formatGames(gamesCategories[CURRENT_CATEGORY], user),
    futureGames: formatGames(gamesCategories[FUTURE_CATEGORY], user),
    players: {
      loading: friends.loading,
      error: friends.error,
      list: formatFriends(friendsList),
    },
    submissionData,
    shouldLoad: shouldLoad(loading, error),
  };
};

export default mapData;