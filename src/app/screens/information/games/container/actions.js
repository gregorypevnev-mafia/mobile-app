import { bindActionCreators } from "redux";
import {
  loadGames,
  createGame,
  deleteGame,
  acceptInvitation,
  rejectInvitation
} from "../../../../../store/games/actions";
import { loadFriends } from "../../../../../store/friends/actions";

const mapActions = dispatch => bindActionCreators({
  loadGames,
  createGame,
  deleteGame,
  acceptInvitation,
  rejectInvitation,
  loadFriends,
}, dispatch);

export default mapActions;
