import React, { useEffect } from "react";
import { View, StyleSheet } from "react-native";
import { connect } from "react-redux";
import Page from "../../../components/layout/Page";
import Heading from "../../../components/common/Heading";
import InvitationsList from "./components/invitations/InvitationsList";
import GamesList from "./components/games/GamesList";
import GameForm from "../../../components/containers/game-form/GameForm";
import useDialog from "../../../hooks/dialog";
import { mapStateToProps, mapDispatchToProps } from "./container";

const styles = StyleSheet.create({
  list: {
    height: 225,
    marginBottom: 15
  }
});

const GamesScreen = ({
  loading,
  submitting,
  error,
  submissionError,
  players,
  invitations,
  pastGames,
  currentGames,
  futureGames,
  submissionData,
  shouldLoad,
  loadGames,
  createGame,
  deleteGame,
  acceptInvitation,
  rejectInvitation,
  loadFriends,
  playGame
}) => {
  const [isOpen, { openDialog, closeDialog }] = useDialog();

  useEffect(() => {
    if (shouldLoad) loadGames();
  }, []);

  return (
    <Page
      dialog={(
        <GameForm
          submitting={submitting}
          error={submissionError}
          defaults={submissionData}
          players={players}
          onCancel={closeDialog}
          onSubmit={async data => {
            const result = await createGame(data);

            if (result) closeDialog();
          }}
        />
      )}
      showDialog={isOpen}
      onCloseDialog={closeDialog}
      onOpenDialog={() => {
        loadFriends();
        openDialog();
      }}
      items={[
        {
          name: "invitations",
          content: (<View style={styles.list}>
            <Heading>Invitations</Heading>
            <InvitationsList
              loading={loading}
              error={error}
              invitations={invitations}
              onAccepted={acceptInvitation}
              onRejected={rejectInvitation}
            />
          </View>)
        },
        {
          name: "ongoing",
          content: (
            <View style={styles.list}>
              <Heading>Ongoing</Heading>
              <GamesList
                loading={loading}
                error={error}
                games={currentGames}
                onPlay={playGame}
              />
            </View>
          )
        },
        {
          name: "future",
          content: (
            <View style={styles.list}>
              <Heading>Future</Heading>
              <GamesList
                loading={loading}
                error={error}
                games={futureGames}
                onDelete={deleteGame}
              />
            </View>
          )
        },
        {
          name: "past",
          content: (
            <View style={styles.list}>
              <Heading>Past</Heading>
              <GamesList
                loading={loading}
                error={error}
                games={pastGames}
              />
            </View>
          )
        }
      ]}
    />
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(GamesScreen);