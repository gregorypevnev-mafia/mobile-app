import React from "react";
import { View, Text, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: "row",
    flexGrow: 0,
    flexShrink: 0,
  },
  readyStats: {
    fontSize: 16
  },
  separator: {
    marginHorizontal: 2,
    fontSize: 16
  },
  totalStats: {
    fontSize: 16,
    fontWeight: "bold"
  }
})

const Stats = ({ waiting, ready }) => {
  if (waiting === 0) return (
    <Text>Preparing...</Text>
  );

  return (
    <View style={styles.wrapper}>
      <Text style={styles.readyStats}>{ready}</Text>
      <Text style={styles.separator}>/</Text>
      <Text style={styles.totalStats}>{waiting + ready}</Text>
    </View>
  );
}

export default Stats;
