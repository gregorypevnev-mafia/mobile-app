import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Button from "../../../../../components/common/Button";
import Status from "./Status";
import Stats from "./Stats";
import resources from "../../../../../resources";
import { isCancelled, isPlaying, isWaiting, isFinished } from "../../../../../../utilities/game";
import { toDate, timeInfo } from "../../../../../../utilities/time";

const styles = StyleSheet.create({
  game: {
    flex: 1,
    padding: 15,
    width: 250,
    backgroundColor: "#fefefe",
    ...resources.shadow
  },
  gameTitle: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10
  },
  gameContent: {
    marginVertical: 10,
    marginBottom: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  gameText: {
    fontSize: 16,
  },
  gameActions: {
    alignSelf: "center"
  }
});

const Game = ({
  game: { name, by, status, result, waiting, ready, at },
  owned,
  deleting,
  onPlay,
  onDelete
}) => (
    <View style={styles.game}>
      <Text style={styles.gameTitle}>{name} by {by}</Text>
      <View style={styles.gameContent}>
        <Status status={status} result={result} />
        {isWaiting(status) && <Stats waiting={waiting} ready={ready} />}
      </View>
      <View style={styles.gameActions}>
        {isPlaying(status) && <Button onPress={() => onPlay && onPlay()}>Play</Button>}
        {(owned && isCancelled(status)) && (
          <Button
            disabled={deleting}
            onPress={() => onDelete && onDelete()}
            danger
          >
            {deleting ? "Deleting..." : "Delete"}
          </Button>
        )}
      </View>
      {isFinished(status) && <Text>Played: {timeInfo(at)}</Text>}
    </View>
  );

export default Game;
