import React from "react";
import { View, FlatList, StyleSheet } from "react-native";
import Game from "./Game";
import { listStyles } from "../../../styles";
import withPlaceholder from "../../../../../components/hoc/withPlaceholder";

const styles = StyleSheet.create({
  item: {
    padding: 5,
    alignSelf: "stretch"
  }
});

const GamesList = ({ games, onPlay, onDelete }) => (
  <View style={listStyles.list}>
    <FlatList
      data={games}
      keyExtractor={game => game.id}
      renderItem={({ item }) => (
        <View style={styles.item}>
          <Game
            game={item.game}
            deleting={item.deleting}
            owned={item.isOwned}
            onPlay={() => onPlay && onPlay(item.id)}
            onDelete={() => onDelete && onDelete(item.id)}
          />
        </View>
      )}
      horizontal
      showsHorizontalScrollIndicator={false}
    />
  </View>
);

const placeholderSettings = {
  list: "games",
  errorMessage: "Could not load games",
  emptyMessage: "No games",
};

export default withPlaceholder(placeholderSettings)(GamesList);
