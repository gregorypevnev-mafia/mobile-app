import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { GAMES_STATUSES, gameStatusMessage } from "../../../../../../utilities/game";

const styles = StyleSheet.create({
  wrapper: {
    marginVertical: 5
  },
  status: {
    fontSize: 16
  }
});

const regularStatusStyle = StyleSheet.compose(styles.status, { color: "#333" });
const successStatusStyle = StyleSheet.compose(styles.status, { color: "#44cc55" });
const errorStatusStyle = StyleSheet.compose(styles.status, { color: "#ff6611" });

const DEFAULT_STATUS_STYLE = regularStatusStyle;

const STATUS_STYLES = {
  [GAMES_STATUSES.waiting]: regularStatusStyle,
  [GAMES_STATUSES.cancelled]: errorStatusStyle,
  [GAMES_STATUSES.playing]: successStatusStyle,
  [GAMES_STATUSES.finished]: regularStatusStyle,
};

const gameStatusStyle = status => STATUS_STYLES[status] || DEFAULT_STATUS_STYLE

const Status = ({ status, result }) => (
  <View style={styles.wrapper}>
    {result ? (
      <Text style={result.success ? successStatusStyle : errorStatusStyle}>
        {result.message}
      </Text>
    ) : (
        <Text style={gameStatusStyle(status)}>
          {gameStatusMessage(status)}
        </Text>
      )}
  </View>
);

export default Status;
