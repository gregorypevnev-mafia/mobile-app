import React from "react";
import { View, FlatList, StyleSheet } from "react-native";
import Invitation from "./Invitation";
import { listStyles } from "../../../styles";
import withPlaceholder from "../../../../../components/hoc/withPlaceholder";

const styles = StyleSheet.create({
  item: {
    padding: 5
  }
});

const InvitationsList = ({ invitations, onAccepted, onRejected }) => (
  <View style={listStyles.list}>
    <FlatList
      data={invitations}
      keyExtractor={invitation => invitation.id}
      renderItem={({ item }) => (
        <View style={styles.item}>
          <Invitation
            invitation={item.invitation}
            accepting={item.accepting}
            rejecting={item.rejecting}
            onAccept={() => onAccepted(item.id)}
            onReject={() => onRejected(item.id)}
          />
        </View>
      )}
      horizontal
      showsHorizontalScrollIndicator={false}
    />
  </View>
);

const placeholderSettings = {
  list: "invitations",
  errorMessage: "Could not load invitations",
  emptyMessage: "No invitations to new games",
};

export default withPlaceholder(placeholderSettings)(InvitationsList);
