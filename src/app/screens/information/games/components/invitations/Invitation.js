import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Button from "../../../../../components/common/Button";
import resources from "../../../../../resources";
import { toTime } from "../../../../../../utilities/time";

const styles = StyleSheet.create({
  invitation: {
    flex: 1,
    padding: 12,
    width: 250,
    backgroundColor: "#fefefe",
    ...resources.shadow,
  },
  invitationTitle: {
    fontSize: 18,
    fontWeight: "bold"
  },
  invitationContent: {
    marginVertical: 10
  },
  invitationText: {
    fontSize: 16,
  },
  invitationActions: {
    flexDirection: "row",
    justifyContent: "space-between"
  }
});

const Invitation = ({
  invitation: { name, by, at },
  accepting,
  rejecting,
  onAccept, onReject
}) => {
  const isProcessing = accepting || rejecting;

  return (
    <View style={styles.invitation}>
      <Text style={styles.invitationTitle}>{name}</Text>
      <View style={styles.invitationContent}>
        <Text style={styles.invitationText}>From: {by}</Text>
        <Text style={styles.invitationText}>At: {toTime(at)}</Text>
      </View>
      <View style={styles.invitationActions}>
        <Button
          disabled={isProcessing}
          onPress={() => onAccept && onAccept()}
        >
          {accepting ? "Accepting..." : "Accept"}
        </Button>
        <Button
          disabled={isProcessing}
          onPress={() => onReject && onReject()}
          danger
        >
          {rejecting ? "Rejecting..." : "Reject"}
        </Button>
      </View>
    </View>
  );
}

export default Invitation;
