export const primary = "#ff9933";

export const secondary = "#3333ff";

export const dark = "#333";

export const light = "#fafafa";
