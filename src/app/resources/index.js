import * as device from "./device";
import * as colors from "./colors";
import * as sizes from "./sizes";
import { shadow } from "./shadow";

export default {
  device,
  colors,
  sizes,
  shadow,
};
