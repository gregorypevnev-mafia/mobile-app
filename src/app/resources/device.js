import { StyleSheet, Platform } from "react-native";

const sharedStyles = StyleSheet.create({
  app: {
    flex: 1
  }
});

const androidParams = {
  marginTop: 20
};

const webParams = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  maxWidth: 420,
  maxHeight: 720,
  width: "100%",
  height: "100%",
  border: "1px solid #333"
};

export const appDeviceStyle = () => {
  switch (Platform.OS) {
    case "android":
      return StyleSheet.compose(sharedStyles.app, androidParams);
    case "web":
      return StyleSheet.compose(sharedStyles.app, webParams);
    default:
      return sharedStyles.app;
  }
};