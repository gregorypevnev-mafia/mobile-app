import React, { Component } from "react";
import { View } from "react-native";
import { Provider } from "react-redux";
import Navigator from "./navigation/AppNavigator";
import resources from "./resources";

const appStyle = resources.device.appDeviceStyle();

// App should ALWAYS be a Class-Component
class App extends Component {
  constructor(props) {
    super(props);

    this.state = { store: null };

    props.store.then(store => this.setState({ store }))
  }
  render() {
    const { store } = this.state;

    if (store === null) return false; // Almost instant

    return (
      <View style={appStyle}>
        <Provider store={store}>
          <Navigator />
        </Provider>
      </View>
    );
  }
}

export default App;