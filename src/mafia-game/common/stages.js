const {
  TOWN_PHASE,
  TRIAL_PHASE,
  DAY_STANDBY,
  MAFIA_PHASE,
  POLICE_PHASE,
  DOCTOR_PHASE,
  NIGHT_STANDBY,
  SKIP_ACTION,
  KILL_ACTION,
  EXPOSE_ACTION,
  SAVE_ACTION,
  EXECUTE_ACTION,
  MAFIA_ROLE,
  POLICE_ROLE,
  DOCTOR_ROLE
} = require("./constants");

const STARTING_STEP = 1;

const PHASES_DETAILS = {
  [TOWN_PHASE]: {
    title: "Morning",
    action: SKIP_ACTION,
    role: null,
    skippable: true,
    order: 0,
    duration: 60,
  },
  [TRIAL_PHASE]: {
    title: "Trial",
    action: EXECUTE_ACTION,
    role: null,
    order: 1,
    duration: 30
  },
  [DAY_STANDBY]: {
    title: "Day is over",
    action: null,
    role: null,
    standby: true,
    order: 2,
    duration: 10,
  },
  [MAFIA_PHASE]: {
    title: "Mafia's turn",
    action: KILL_ACTION,
    role: MAFIA_ROLE,
    order: 3,
    duration: 30,
  },
  [POLICE_PHASE]: {
    title: "Police's turn",
    action: EXPOSE_ACTION,
    role: POLICE_ROLE,
    order: 4,
    duration: 30,
  },
  [DOCTOR_PHASE]: {
    title: "Doctor's turn",
    action: SAVE_ACTION,
    role: DOCTOR_ROLE,
    order: 5,
    duration: 30,
  },
  [NIGHT_STANDBY]: {
    title: "Night is over",
    action: null,
    role: null,
    standby: true,
    order: 6,
    duration: 10,
  }
};

const PHASES_ORDER = Object.keys(PHASES_DETAILS)
  .map(name => ({
    name,
    order: PHASES_DETAILS[name].order
  }))
  .sort((phase1, phase2) => phase1 - phase2)
  .map(({ name }) => name);

const phaseDetails = phaseName => {
  const phase = PHASES_DETAILS[phaseName];

  if (!phase) throw new Error("Invalid name of the phase");

  return phase;
};

const startingPhase = () => TOWN_PHASE;

const isStartingPhase = phase => phase === TOWN_PHASE;

const nextPhase = phaseName => {
  const nextOrder = PHASES_DETAILS[phaseName].order + 1;
  const isFinalPhase = nextOrder === PHASES_ORDER.length;
  const nextPhase = PHASES_ORDER[nextOrder % PHASES_ORDER.length];

  return {
    isFinalPhase,
    nextPhase,
  }
};

const initialStage = () => ({
  step: STARTING_STEP,
  phase: startingPhase(),
});

const nextStage = ({ step, phase }) => {
  if (step === STARTING_STEP && isStartingPhase(phase))
    // Skipping trial on the first day
    return {
      step: STARTING_STEP,
      phase: DAY_STANDBY,
    };

  const { isFinalPhase, nextPhase: nextPhaseName } = nextPhase(phase);
  const nextStepNumber = isFinalPhase ? step + 1 : step;

  return {
    step: nextStepNumber,
    phase: nextPhaseName,
  };
};

module.exports = {
  phaseDetails,
  startingPhase,
  isStartingPhase,
  initialStage,
  nextStage,
};
