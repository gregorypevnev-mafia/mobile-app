const { PLAYER_ALIVE, PLAYER_DEAD, PLAYER_EXPOSED } = require("./common/constants");
const { phaseDetails } = require("./common/stages");
const { roleName, populateRoles } = require("./roles");
const { shuffleList } = require("./utils");

const PLAYER_STATUS_NAMES = {
  [PLAYER_ALIVE]: "Alive",
  [PLAYER_DEAD]: "Dead",
  [PLAYER_EXPOSED]: "Exposed",
};

const isPlayerActive = ({ phase }, { role, status }) => {
  if (status === PLAYER_DEAD) return false;

  const details = phaseDetails(phase);

  if (details.standby) return false;

  return details.role === null || details.role === role;
};

const playerInfo = ({ role, status }, isPublic) => {
  const displayRole = isPublic || status !== PLAYER_ALIVE;

  return {
    role: displayRole ? roleName(role) : "Unknown",
    status: PLAYER_STATUS_NAMES[status] || "Unknown",
  };
};

const isVotable = ({ role, status }, phaseRole, standby) => {
  // Standy -> Cannot Vote
  if (standby) return false;

  // Cannot vote for dead players
  if (status === PLAYER_DEAD) return false;

  // Voting for opposite roles / All roles
  return role !== phaseRole || !phaseRole;
}

const votablePlayers = ({ phase }, players) => {
  const { role, standby } = phaseDetails(phase);

  return players.map(data => ({
    ...data,
    votable: isVotable(data, role, standby),
  }));
};

const initializePlayers = (players, roles) => {
  const rolesList = shuffleList(populateRoles(roles));

  return players.reduce((players, { id }, i) => ({
    ...players,
    [id]: {
      id,
      role: rolesList[i],
      status: PLAYER_ALIVE,
    }
  }));
};

module.exports = {
  playerInfo,
  isPlayerActive,
  initializePlayers,
  votablePlayers,
};
