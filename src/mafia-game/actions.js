const {
  KILL_ACTION,
  EXPOSE_ACTION,
  SAVE_ACTION,
  EXECUTE_ACTION,
  SKIP_ACTION,
  PLAYER_ALIVE,
  PLAYER_DEAD,
  PLAYER_EXPOSED
} = require("./common/constants");
const { phaseDetails } = require("./common/stages");

const ACTION_DETAILS = {
  [KILL_ACTION]: "Kill",
  [EXPOSE_ACTION]: "Inspect",
  [SAVE_ACTION]: "Save",
  [EXECUTE_ACTION]: "Accuse",
  [SKIP_ACTION]: "Continue",
};

const actionForStage = ({ phase }) => {
  const { action, skippable, standby } = phaseDetails(phase);

  return {
    title: ACTION_DETAILS[action],
    skippable: skippable || false,
    waiting: standby || false,
  };
};

const processActions = actions => actions.reduce((result, { action, target }) => {
  switch (action) {
    case KILL_ACTION:
      return {
        [target]: PLAYER_DEAD,
        ...result,
      };
    case EXPOSE_ACTION:
      return {
        [target]: result[target] !== PLAYER_DEAD ? PLAYER_EXPOSED : result[target]
      }
    case SAVE_ACTION:
      return {
        [target]: result[target] === PLAYER_DEAD ? PLAYER_ALIVE : result[target],
        ...result,
      };
    case EXECUTE_ACTION:
      return {
        [target]: PLAYER_DEAD,
        ...result,
      };
  }
}, {});

module.exports = { actionForStage, processActions };
