const {
  MAFIA_RESULT,
  TOWN_RESULT,
} = require("./common/constants");
const {
  phaseDetails,
  startingPhase,
  nextStage,
} = require("./common/stages");
const { processActions } = require("./actions");
const { countRoles } = require("./roles");

const STARTING_STEP = 1;

const initialStage = () => ({
  step: STARTING_STEP,
  phase: startingPhase(),
});

const next = ({ stage, ...details }) => ({
  stage: nextStage(stage),
  ...details,
});

const isStandby = ({ phase }) => {
  const phaseData = phaseDetails(phase);

  return phaseData.standby;
}

const gameInfo = ({ id, details, step, phase }) => ({
  id,
  step,
  phase: phaseDetails(phase).title,
  ...details,
});

const phaseDuration = phase => {
  const { duration, standby } = phaseDetails(phase);

  if (standby) return 0;

  return Number(duration) || 0;
}

const phaseName = phase => String(phaseDetails(phase).title);

// IMPORTANT: Separating Play-Phase and Standby-Phase for Play-Service
//  - NO Encapsulation
//  -> Allows determining whether passing Votes or Actions => Clearing Votes or Actions / Adding Action or 

const selectTarget = votes =>
  votes.reduce(({ max, counts }, { target }) => {
    counts[target] = (counts[target] || 0) + 1;

    return {
      max: counts[target] > counts[max] ? target : max,
      counts,
    }
  }, {
    max: null,
    counts: {},
  }).max;

// Returning an action
const processPlay = ({ phase }, votes) => {
  const phaseData = phaseDetails(phase);

  if (phaseData.standby) throw new Error("Not a Play Step");

  return {
    action: phaseData.action,
    target: selectTarget(votes),
  };
};

// Returning updated list of players
const processStandby = ({ phase }, actions) => {
  const phaseData = phaseDetails(phase);

  if (!phaseData.standby) throw new Error("Not a Standby Step");

  const changes = processActions(actions);

  return changes;
};

const applyChange = (players, id, changedStatus) => {
  const targetPlayer = players[id];

  if (!targetPlayer) return players;

  return {
    ...players,
    [id]: {
      ...targetPlayer,
      status: changedStatus,
    }
  };
}

const applyChanges = (players, changes) =>
  Object.keys(changes || {}).reduce(
    (players, playerId) => applyChange(players, playerId, changes[playerId]),
    players
  );

const validateGame = ({ roles, players }) =>
  countRoles(roles) === players.length;

const hasMafiaWon = result =>
  result === MAFIA_RESULT;

const hasTownWon = result =>
  result === TOWN_RESULT;

module.exports = {
  initialStage,
  gameInfo,
  phaseDuration,
  phaseName,
  next,
  isStandby,
  processPlay,
  processStandby,
  applyChanges,
  validateGame,
  hasMafiaWon,
  hasTownWon,
};
