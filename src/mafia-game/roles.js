const { MAFIA_ROLE, POLICE_ROLE, DOCTOR_ROLE, CIVILIAN_ROLE } = require("./common/constants");

const DEFAULT_TITLE = "Unknown";

const ROLE_DETAILS = {
  [MAFIA_ROLE]: {
    title: "Mafia Member",
    min: 1,
    max: 3,
    default: 1,
  },
  [POLICE_ROLE]: {
    title: "Police Officer",
    min: 1,
    max: 2,
    default: 1,
  },
  [DOCTOR_ROLE]: {
    title: "Doctor",
    min: 1,
    max: 2,
    default: 1,
  },
  [CIVILIAN_ROLE]: {
    title: "Civilian",
    min: 0,
    max: 9,
    default: 0,
  },
}; // Maximum 16 players in total

const roleName = (name, plural = false) => {
  const role = ROLE_DETAILS[name];

  return `${(role ? role.title : DEFAULT_TITLE)}${plural ? "s" : ""}`
};

// { [Role]: Count }
const rolesToList = (roles = {}) =>
  Object.keys(ROLE_DETAILS).map(roleName => ({
    name: roleName,
    count: roles[roleName] || ROLE_DETAILS[roleName].default,
    min: ROLE_DETAILS[roleName].min,
    max: ROLE_DETAILS[roleName].max,
  }));

// [{ name, count, ... }]
const rolesFromList = roles =>
  roles.reduce((mapped, { name, count }) => ({
    ...mapped,
    [name]: count
  }), {});

const isValidRole = (count, { min, max }) => (
  Number(count) >= Number(min) &&
  Number(count) <= Number(max)
);

const validateRoles = roles =>
  Object.keys(ROLE_DETAILS)
    .every(roleName => {
      if (!(roleName in roles)) return false; // Specifying each role is necessary

      return isValidRole(roles[roleName], ROLE_DETAILS[roleName]);
    });

const initializeRoles = () =>
  Object.keys(ROLE_DETAILS).reduce((roles, roleName) => ({
    ...roles,
    [roleName]: ROLE_DETAILS[roleName].default,
  }), {});

// Providing a list with each item being a single instance of a role
const populateRole = (role, count) => {
  const array = [];

  for (let i = 0; i < count; i++)
    array.push(role);

  return array;
}

// { [Role]: Count }
const populateRoles = roles =>
  Object.keys(roles)
    .reduce(
      (rolesList, role) => [...rolesList, ...populateRole(role, roles[role])],
      []
    );

const countRoles = roles =>
  Object.keys(roles).reduce((total, role) => total + roles[role], 0);

module.exports = {
  roleName,
  rolesToList,
  rolesFromList,
  validateRoles,
  initializeRoles,
  populateRoles,
  countRoles,
};
