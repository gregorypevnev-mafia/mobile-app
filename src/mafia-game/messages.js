const {
  TOWN_PHASE,
  TRIAL_PHASE,
  DAY_STANDBY,
  MAFIA_PHASE,
  POLICE_PHASE,
  DOCTOR_PHASE,
  NIGHT_STANDBY,
  KILL_ACTION,
  EXPOSE_ACTION,
  SAVE_ACTION,
  EXECUTE_ACTION,
  PLAYER_DEAD
} = require("./common/constants");
const { roleName } = require("./roles");

const FIRST_END = "The town gets to know each other";

const PHASE_MESSAGES = {
  [TOWN_PHASE]: {
    start: "The town wakes up",
    end: "The town gathers together"
  },
  [TRIAL_PHASE]: {
    start: "The trial begins",
    end: "The trial is over"
  },
  [DAY_STANDBY]: {
    start: "The day ends",
    end: "The night begins"
  },
  [MAFIA_PHASE]: {
    start: "Mafia members wake up",
    end: "Mafia members go to sleep"
  },
  [POLICE_PHASE]: {
    start: "Police officers wake up",
    end: "Police officers go to sleep"
  },
  [DOCTOR_PHASE]: {
    start: "Doctors wake up",
    end: "Doctors go to sleep"
  },
  [NIGHT_STANDBY]: {
    start: "The night ends",
    end: "The day begins"
  }
};

const stageStartMessage = ({ phase }) =>
  PHASE_MESSAGES[phase] ? PHASE_MESSAGES[phase].start : null;

const stageEndMessage = ({ phase, step }) => {
  if (step === 1 && phase === TOWN_PHASE)
    return FIRST_END;

  return PHASE_MESSAGES[phase] ? PHASE_MESSAGES[phase].end : null;
}

const executedMessage = ({ username, role }) =>
  `${username} (${roleName(role)}) was executed in trial`;

const killedMessage = ({ id, username, role }, changes) => {
  if (changes[id] === PLAYER_DEAD)
    return `${username} (${roleName(role)}) was killed by mafia`;

  return `${username} was saved from assasination by doctors`;
};

const exposedMessage = ({ username, role }) =>
  `${username} was inspected by the police and turned out to be ${roleName(role)}`;

const savedMessage = ({ username }) =>
  `${username} was visited by doctors`;

const ACTION_MESSAGES = {
  [EXECUTE_ACTION]: executedMessage,
  [KILL_ACTION]: killedMessage,
  [EXPOSE_ACTION]: exposedMessage,
  [SAVE_ACTION]: savedMessage
};

const actionMessages = (actions, changes, players) =>
  actions
    .filter(({ action, target }) => !!ACTION_MESSAGES[action] && players[target])
    .map(({ action, target }) => ACTION_MESSAGES[action](players[target], changes));

const eventsMessage = ({ phase }) => {
  if (phase === DAY_STANDBY)
    return "The trial results:";

  if (phase === NIGHT_STANDBY)
    return "The night events:";

  return "Here's what happended";
}

module.exports = {
  stageStartMessage,
  actionMessages,
  stageEndMessage,
  eventsMessage,
};
