const games = require("./games");
const players = require("./players");
const roles = require("./roles");
const actions = require("./actions");
const messages = require("./messages");

module.exports = {
  games,
  players,
  roles,
  actions,
  messages,
};
