import React from "react";
import 'expo/build/Expo.fx';
import registerRootComponent from 'expo/build/launch/registerRootComponent';
import { activateKeepAwake } from 'expo-keep-awake';
import Constants from "expo-constants";
import buildStore from "./src/store";
import App from "./src/app"

const CONFIG = Constants.manifest.extra;

if (__DEV__) {
  activateKeepAwake();
}

const store = buildStore({
  apiURL: CONFIG.API_URL,
  wsURL: CONFIG.WS_URL,
  ticketURL: CONFIG.TICKET_URL,
});

// MUST BE REGISTERED IMMEDIATELY -> Provide Promise
registerRootComponent(() => <App store={store} />);